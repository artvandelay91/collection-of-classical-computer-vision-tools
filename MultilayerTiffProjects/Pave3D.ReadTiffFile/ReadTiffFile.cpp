#include <tiffio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace cv;
using namespace std;


#define N(a) (sizeof(a) / sizeof (a[0]))
#define INTENSITYLAYER  65000 
#define INTENSITYLEFTLAYER  65001
#define INTENSITYRIGHTLAYER  65002
#define RANGELAYER 65003
#define RANGELEFTLAYER  65004
#define RANGERIGHTLAYER  65005
#define DATASOURCE 65006
#define MODELTRANSFORMATIONMATRIX 34264
#define GEOKEYDIRECTORY 34735
#define GEOLICENSE 34737

#define INTENSITY_PAGE 0
#define RANGE_PAGE 1
#define INTENSITY_LEFT_PAGE 2
#define INTENSITY_RIGHT_PAGE 3
#define RANGE_LEFT_PAGE 4
#define RANGE_RIGHT_PAGE 5
const char* SENSOR_NAME =  "LCMS";

static TIFFExtendProc parent_extender = NULL;

static void registerCustomTIFFTags(TIFF *tif)
{
   static const TIFFFieldInfo xtiffFieldInfo[] =
	{
		{INTENSITYLAYER,  1, 1, TIFF_BYTE,  FIELD_CUSTOM, 0, 0, const_cast<char*>("Intensity") },
        {INTENSITYLEFTLAYER,  1, 1, TIFF_BYTE,  FIELD_CUSTOM, 0, 0, const_cast<char*>("IntensityLeft") },
        {INTENSITYRIGHTLAYER,  1, 1, TIFF_BYTE, FIELD_CUSTOM, 0, 0, const_cast<char*>("IntensityRight") },
        {RANGELAYER,  1, 1, TIFF_BYTE, FIELD_CUSTOM, 0, 0, const_cast<char*>("Range") },
        {RANGELEFTLAYER,  1, 1, TIFF_BYTE, FIELD_CUSTOM, 0, 0, const_cast<char*>("RangeLeft") },
		{RANGERIGHTLAYER,  1, 1, TIFF_BYTE, FIELD_CUSTOM, 0, 0, const_cast<char*>("RangeRight") },
		{DATASOURCE,  -1, -1, TIFF_ASCII, FIELD_CUSTOM, 0, 0, const_cast<char*>("Source") },
		{MODELTRANSFORMATIONMATRIX, 16, 16, TIFF_DOUBLE, FIELD_CUSTOM, 0, 0, const_cast<char*>("ModelTransformationTag")},
		{GEOKEYDIRECTORY, 24, 24, TIFF_SHORT, FIELD_CUSTOM, 0, 0, const_cast<char*>("GeoKeyDirectoryTag")},
		{GEOLICENSE, -1, -1, TIFF_ASCII, FIELD_CUSTOM, 0, 0, const_cast<char*>("GeoAsciiParamsTag")}
	};
	
	/* Install the extended Tag field info */
    int error = TIFFMergeFieldInfo(tif, xtiffFieldInfo, N(xtiffFieldInfo));

    if (parent_extender)
        (*parent_extender)(tif);
}


void readFromTiff( TIFF *tif, uint8 *intensityLayer, uint8 *intensityLeftLayer, uint8 *intensityRightLayer, 
				  uint8 *rangeLayer, uint8 *rangeLeftLayer, uint8 *rangeRightLayer, char* sensorName, 
				  double *modelTFMatrix, short *geoKeys, char *geoLicense)
{
    // Create the TIFF directory object:
    

    // Read in the image size and metadata:
	TIFFGetField(tif, INTENSITYLAYER, intensityLayer);
	TIFFGetField(tif, INTENSITYLEFTLAYER, intensityLeftLayer);
	TIFFGetField(tif, INTENSITYRIGHTLAYER, intensityRightLayer);
    TIFFGetField(tif, RANGELAYER, rangeLayer);
	TIFFGetField(tif, RANGELEFTLAYER, rangeLeftLayer);
	TIFFGetField(tif, RANGERIGHTLAYER, rangeRightLayer);
	TIFFGetField(tif, RANGERIGHTLAYER, sensorName);
	TIFFGetField(tif, MODELTRANSFORMATIONMATRIX, modelTFMatrix);
	TIFFGetField(tif, GEOKEYDIRECTORY, geoKeys);
	TIFFGetField(tif, GEOLICENSE, geoLicense);
}


int main()
{
	//create array of mat for number of layers
	const int nLayers = 6;
	Mat layers[nLayers];
	
	//open the tif file
	string image_path  = "E:\\DATA\\Vision\\fis to geotif\\geotif\\";
	string image_name = "000007650000.tif";
	//parent_extender = TIFFSetTagExtender(registerCustomTIFFTags);
	TIFF *tif = TIFFOpen((image_path+image_name).c_str(), "r");	
	
	bool firstLayer = true;
	
	if (tif) 
	{
        for(int layer=0; layer < nLayers; layer++)
		{			
			if (firstLayer)
			{
				uint8 intensityLayer; 
				uint8 intensityLeftLayer;
				uint8 intensityRightLayer; 
				uint8 rangeLayer;
				uint8 rangeLeftLayer;
				uint8 rangeRightLayer;
				char* sensorName = new char[1000]; 
				double *modelTFMatrix = new double[16];
				short *geoKeys = new short[24]; 
				char *geoLicense = new char[1000];
				readFromTiff( tif,	&intensityLayer, &intensityLeftLayer, &intensityRightLayer, 
									&rangeLayer, &rangeLeftLayer, &rangeRightLayer, sensorName, 
									modelTFMatrix, geoKeys, geoLicense);
			}
			
			//get number of rows and cols
			int nRows, nCols;
			TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &nRows);
			TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &nCols);



			//Read the layer data scanline by scanline
			uint8 *layerData = (uint8*) _TIFFmalloc(nRows*nCols * sizeof (uint8));		
			uint8 *rowData = (uint8*) _TIFFmalloc(nCols *sizeof(uint8));
			for (int row = 0; row < nRows; row++)
			{
				TIFFReadScanline(tif, rowData, row);
				memcpy(layerData + row*nCols, rowData, nCols*sizeof(uint8));
			}
			
			//create a mat with the layer data
			layers[layer] = Mat(nRows, nCols, CV_8UC1, layerData);			
			
			//write the mat to disk
			string imName = "img_" + to_string(static_cast<long long>(layer)) + ".bmp";
			imwrite(imName, layers[layer]);
			
			//Free the memory and close	
			_TIFFfree(layerData);
			_TIFFfree(rowData);
		}
            
		TIFFClose(tif);
    }
	return 0;
}