#include <iostream>
#include <tiffio.h>
#include <ppl.h>
#include "opencv2\opencv.hpp"
#include "timeProfiler.h"
#include <string>

using namespace std;
using namespace cv;
using namespace concurrency;

//Write core tif tags for the layer
void setCoreTiffTags(TIFF *tiffdata, int page_num, Mat layer, size_t nLayers, int jpeg_quality)
{
	//TIFFSetField(tiffdata, TIFFTAG_IMAGEWIDTH, layer.cols);
	TIFFSetField(tiffdata, TIFFTAG_IMAGELENGTH, layer.rows);
	TIFFSetField(tiffdata, TIFFTAG_SAMPLESPERPIXEL, 1);
	TIFFSetField(tiffdata, TIFFTAG_BITSPERSAMPLE, 8);			
	TIFFSetField(tiffdata, TIFFTAG_PHOTOMETRIC, 1);
	TIFFSetField(tiffdata, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
	TIFFSetField(tiffdata, TIFFTAG_PAGENUMBER, page_num, nLayers);	
	TIFFSetField(tiffdata, TIFFTAG_ROWSPERSTRIP, 8);

	TIFFSetField(tiffdata, TIFFTAG_COMPRESSION, COMPRESSION_JPEG);
	TIFFSetField(tiffdata, TIFFTAG_JPEGQUALITY, jpeg_quality);	
}

//Write the image data for the layer
void writeImageData(TIFF *tiffdata, Mat layer)
{
	unsigned char *buf = NULL;

	if (TIFFScanlineSize(tiffdata))
		buf =(unsigned char *)_TIFFmalloc(layer.cols);
	else
		buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(tiffdata));

	for (uint32 row = 0; row < layer.rows; row++)
	{
		memcpy(buf, &layer.data[(layer.rows-row-1)*layer.cols], layer.cols);
		if (TIFFWriteScanline(tiffdata, buf, row, 0) < 0)
			break;
	}
	if (buf)
		_TIFFfree(buf);
}

//Create multipage tiff sequentially
void saveAsPave3DTiff(string saveName, Mat *layers, size_t nLayers, int jpeg_quality)
{	
	Mat bigImage;
	hconcat(layers, nLayers, bigImage);
	string newName  = saveName + ".jpg";
	vector<int> params; params.push_back(CV_IMWRITE_JPEG_QUALITY); params.push_back(jpeg_quality);
	imwrite(newName, bigImage, params);

	//PROFILE_START(SINGLE_IMAGE)
	TIFF *tiffdata = TIFFOpen(saveName.c_str(), "w");
	for(int page = 0; page<nLayers; page++)
	{
		string newName  = saveName + "_" + to_string(static_cast<long long>(page)) + ".jpg";
		vector<int> params; params.push_back(CV_IMWRITE_JPEG_QUALITY); params.push_back(jpeg_quality);
		imwrite(newName, layers[page], params);
		//setCoreTiffTags(tiffdata, page, layers[page], nLayers, jpeg_quality);
		///*vector<uchar> buffer; 
		//vector<int> params; params.push_back(CV_IMWRITE_JPEG_QUALITY); params.push_back(jpeg_quality);
		//imencode(".jpg", layers[page], buffer, params);*/
		//TIFFWriteEncodedStrip(tiffdata, 0, layers[page].data, layers[page].rows*layers[page].step[0]);
		////TIFFWriteRawStrip(tiffdata, 0, buffer.data(), buffer.size());		
		//TIFFWriteDirectory(tiffdata);
	}
	//PROFILE_END(SINGLE_IMAGE)
}

//create a fake Pave 3D multiple buffers
void createFakePave3Ddata(Mat image, Mat *layers, size_t nLayers)
{
	for (int i = 0; i < nLayers; i++)
		layers[i] = image;
}


int main()
{	
	//Paths for input & output
	string input_path = "E:\\DATA\\Pave 3D\\Sample Acqisition Images\\";	
	string output_path = "E:\\DATA\\Pave 3D\\Pave3D Output\\";
	
	//Read a pavement image
	string file_name = "A_0000.bmp";
	string full_name = input_path + file_name; 
	Mat image = imread(full_name, 0);

	//create fake Pave 3D data
	Mat *pave3Ddata = new Mat[6];
	size_t nLayers = 4;
	createFakePave3Ddata(image, pave3Ddata, nLayers);
	
	//fake a stream of data	
	int jpeg_quality = 95;	int nFiles = 100;
	PROFILE_START(ALL_FILES)
	parallel_for(size_t(0), size_t(nFiles+1), [&](size_t(i))
	{			
		string save_name = output_path + "output_" + to_string(static_cast<long long>(jpeg_quality)) + "_" + to_string(static_cast<long long>(i));
		saveAsPave3DTiff(save_name, pave3Ddata, nLayers, jpeg_quality);
	});
	PROFILE_END(ALL_FILES)		
	delete[] pave3Ddata;
	return 0;
}