#ifndef __TIMEPROFILER_H__
#define __TIMEPROFILER_H__
#define PROFILE
#define PROFILE_STRING

#ifdef PROFILE
#include <windows.h>
//LARGE_INTEGER profiler_freq;
#define PROFILE_START(tag) \
LARGE_INTEGER start_time##tag;\
QueryPerformanceCounter(&start_time##tag);
#define PROFILE_END(tag)\
 LARGE_INTEGER end_time##tag;\
 static float total_time##tag = 0.0;\
 static int myCounter##tag=0;\
 myCounter##tag++;\
 QueryPerformanceCounter(&end_time##tag);\
 LARGE_INTEGER profiler_freq##tag;\
 QueryPerformanceFrequency(&profiler_freq##tag);\
 float time##tag =(float)(end_time##tag.QuadPart-start_time##tag.QuadPart)/profiler_freq##tag.QuadPart*1000;\
 total_time##tag +=time##tag;\
 printf("\n%s = %1.5fmS Total=%1.5fmS iter = %d Avg. =%1.5fmS",#tag,time##tag,total_time##tag,myCounter##tag,total_time##tag/myCounter##tag);
#else
#define PROFILE_START(tag)
#define PROFILE_END(tag)
#endif

#ifdef PROFILE_STRING
#include <windows.h>
//LARGE_INTEGER profiler_freq;
#define PROFILE_START_STRING(tag)\
LARGE_INTEGER start_time##tag;\
QueryPerformanceCounter(&start_time##tag);
#define PROFILE_END_STRING(tag, outstring)\
 LARGE_INTEGER end_time##tag;\
 static float total_time##tag = 0.0;\
 static int myCounter##tag=0;\
 myCounter##tag++;\
 QueryPerformanceCounter(&end_time##tag);\
 LARGE_INTEGER profiler_freq##tag;\
 QueryPerformanceFrequency(&profiler_freq##tag);\
 float time##tag =(float)(end_time##tag.QuadPart-start_time##tag.QuadPart)/profiler_freq##tag.QuadPart*1000;\
 total_time##tag +=time##tag;\
 sprintf(outstring, "%s = %1.5fmS Total=%1.5fmS iter = %d Avg. =%1.5fmS",#tag,time##tag,total_time##tag,myCounter##tag,total_time##tag/myCounter##tag);
#else
#define PROFILE_START_STRING(tag)
#define PROFILE_END_STRING(tag, outstring)
#endif


 // #ifdef PROFILE
		 // QueryPerformanceFrequency(&profiler_freq);
 // #endif

#endif