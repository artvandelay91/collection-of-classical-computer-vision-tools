#include "opencv2/core/core.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "tiffio.h"
#include <iostream>
#include <string>
#include "ppl.h"
#include <algorithm>

using namespace cv;
using namespace std;
using namespace concurrency;



#define MIN_JPEG 50
#define MAX_JPEG 100

//Write core tif tags for the layer
void setCoreTiffTags(TIFF *tiffdata,Mat layer, int jpeg_quality)
{
	TIFFSetField(tiffdata, TIFFTAG_IMAGEWIDTH, layer.cols);
	TIFFSetField(tiffdata, TIFFTAG_IMAGELENGTH, layer.rows);
	TIFFSetField(tiffdata, TIFFTAG_SAMPLESPERPIXEL, 1);
	TIFFSetField(tiffdata, TIFFTAG_BITSPERSAMPLE, 8);			
	TIFFSetField(tiffdata, TIFFTAG_ORIENTATION, static_cast<int>(ORIENTATION_TOPLEFT));
	TIFFSetField(tiffdata, TIFFTAG_PHOTOMETRIC, 1);
	TIFFSetField(tiffdata, TIFFTAG_ROWSPERSTRIP, 8);
	TIFFSetField(tiffdata, TIFFTAG_COMPRESSION, COMPRESSION_JPEG);
	TIFFSetField(tiffdata, TIFFTAG_JPEGQUALITY, jpeg_quality);
}

//Write the image data for the layer
void writeImageData(TIFF *tiffdata, Mat layer)
{
	unsigned char *buf = NULL;
		
	if (TIFFScanlineSize(tiffdata))
		buf =(unsigned char *)_TIFFmalloc(layer.cols);
	else
		buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(tiffdata));

	for (uint32 row = 0; row < layer.rows; row++)
	{
		memcpy(buf, &layer.data[(layer.rows-row-1)*layer.cols], layer.cols);
		if (TIFFWriteScanline(tiffdata, buf, row, 0) < 0)
			break;
	}
	if (buf)
		_TIFFfree(buf);
}





void BMPtoJpeg(string save_name, Mat bmp, int jpeg_quality)
{
	vector<int> params; params.push_back(IMWRITE_JPEG_QUALITY); params.push_back(jpeg_quality);
	imwrite(save_name, bmp, params);
}

void createPavementJpegs(string input_path, string output_path)
{
	string left_image_name = input_path + "A_0000.bmp";
	string right_image_name = input_path + "B_0000.bmp";
	Mat left_image = imread(left_image_name, 0);
	Mat right_image = imread(right_image_name, 0);	
	parallel_for (size_t(MIN_JPEG), size_t(MAX_JPEG+1), [&](size_t jpeg_quality)
	{
		//JPEG compress left and right images and save		
		cout<<jpeg_quality<<endl;
		string left_save_name = output_path + "left_" + to_string(static_cast<long long>(jpeg_quality)) + ".jpg";
		string right_save_name = output_path + "right_" + to_string(static_cast<long long>(jpeg_quality)) + ".jpg";
		BMPtoJpeg(left_save_name, left_image, jpeg_quality);
		BMPtoJpeg(right_save_name, right_image, jpeg_quality);
	});
}


double calcCovariance(Mat I1, Mat I2)
{
	Mat ip_data(2, I1.rows * I1.cols, I1.type());
	ip_data.row(0) = I1.reshape(0, 1);
	ip_data.row(1) = I2.reshape(0, 1);
	Mat mean, cov;
	calcCovarMatrix(ip_data, cov, mean, CV_COVAR_NORMAL | CV_COVAR_COLS);
	cov = cov/(ip_data.cols-1);
	return cov.at<double>(0, 0);
}

double calcSD(Mat I1)
{
	Mat mean,SD;
	meanStdDev(I1, mean, SD);
	return SD.at<double>(0, 0);
}

double correlationCoefficient(Mat I1, Mat I2)
{
	return (calcCovariance(I1, I2) / (calcSD(I1) * calcSD(I2)));
}

void compareDisparitiesCorrel(string output_path)
{
	string GT_name = output_path + "disparity_ground_truth.bmp";
	Mat GT = imread(GT_name, 0);	

	for (int jpeg_quality = MIN_JPEG; jpeg_quality <= MAX_JPEG; jpeg_quality++)
	{
		string disp_name =  output_path + "disparity_" + to_string(static_cast<long long>(jpeg_quality)) + ".bmp";
		Mat disp_jpeg = imread(disp_name,0);
		cout << correlationCoefficient(GT, disp_jpeg) <<endl;
	}
}




double meanPercentageChange(Mat I1, Mat I2)
{
	Mat diffMat(I1.rows, I1.cols, I1.type()); absdiff(I1, I2, diffMat); 	
	Mat percentageMat(I1.rows, I1.cols, I1.type()); divide(diffMat, I1, percentageMat);
	Scalar meanDiff = mean(percentageMat);
	return meanDiff[0]*100;
}




void compareIntensityCorrel(Mat ground_truth, string compressed_path, string side)
{
	cout<<"Ground_truth"<<'\t'<<norm(ground_truth, ground_truth, NORM_L1)/norm(ground_truth, NORM_L1)<<endl;
	for (int jpeg_quality = MIN_JPEG; jpeg_quality <= MAX_JPEG; jpeg_quality++)
	{
		string jpeg_name =  compressed_path + (side.compare("left") ? "left_" : "right_") + to_string(static_cast<long long>(jpeg_quality)) + ".jpg";
		Mat jpeg_image = imread(jpeg_name, 0);
		Scalar error = norm(ground_truth, jpeg_image, NORM_L1)/norm(ground_truth, NORM_L1);	
		cout<<(side.compare("left") ? "left_" : "right_") + to_string(static_cast<long long>(jpeg_quality)) + ".jpg"<<'\t'<<error[0]<<endl;
	}
}



float depthFun(uchar disparity_pixels)
{
	float focal_length_pixels = 50.1 * (1/0.015);
    float distance_mm = 13 * focal_length_pixels / (float)(disparity_pixels);
    return distance_mm;
}

void create3DFromDisparity(Mat disp, Mat depth_image)
{
	vector<uchar> disp_vec;
	vector<float> depth_vec;
	disp_vec.assign((uchar*)disp.datastart, (uchar*)disp.dataend);
	depth_vec.resize(disp_vec.size());
	transform(disp_vec.begin(), disp_vec.end(), depth_vec.begin(), depthFun);
	memcpy(depth_image.data,depth_vec.data(),depth_vec.size()*sizeof(float));
}


void createDisparity(Mat left_image, Mat right_image, Mat disparity)
{
	int ndisparities = 16*7; 
	int SADWindowSize = 5;
	//Ptr<StereoBM> sbm = StereoBM::create( ndisparities, SADWindowSize );
	//sbm->setMinDisparity(-39);
	//sbm->setPreFilterSize(5);
	//sbm->setPreFilterCap(61);
	//sbm->setTextureThreshold(507);
	//sbm->setUniquenessRatio(0);
	//sbm->setSpeckleWindowSize(0);
	//sbm->setSpeckleRange(8);
	//sbm->setDisp12MaxDiff(1);

	//sbm->compute( left_image, right_image, disparity );	
	/*namedWindow("unnormalized", CV_WINDOW_NORMAL);
	imshow("unnormalized", disparity);	
	waitKey(0);*/





	/*double minVal; double maxVal;
	minMaxLoc( disparity, &minVal, &maxVal );
	Mat imgDisparity8U(disparity.rows, disparity.cols, CV_8UC1);
	disparity.convertTo( imgDisparity8U, CV_8UC1, 255/(maxVal - minVal));
	namedWindow( "normalized", WINDOW_NORMAL );
	imshow( "normalized", imgDisparity8U );
	waitKey(0);*/
}


void createDisparityFromJpegs(string input_path,string output_path)
{
	//Create disparity ground truth
	string left_image_name = input_path + "A_0000.bmp";
	string right_image_name = input_path + "B_0000.bmp";
	Mat left_image = imread(left_image_name, 0);
	Mat right_image = imread(right_image_name, 0);	
	Mat disparity(left_image.rows, left_image.cols, CV_16SC1);
	createDisparity(left_image, right_image, disparity);	
	imwrite((output_path + "disparity_ground_truth.bmp"), disparity);	
	namedWindow("unnormalized", CV_WINDOW_NORMAL);
	imshow("unnormalized", disparity);	
	waitKey(0);

	//Create diaprity from compression level
	parallel_for (size_t(MIN_JPEG), size_t(MAX_JPEG+1), [&](size_t jpeg_quality)
	{	
		string left_save_name = output_path + "left_" + to_string(static_cast<long long>(jpeg_quality)) + ".jpg";
		string right_save_name = output_path + "right_" + to_string(static_cast<long long>(jpeg_quality)) + ".jpg";
		Mat left_jpeg = imread(left_save_name, 0); 
		Mat right_jpeg = imread(right_save_name, 0);
		string disparity_save_name = output_path + "disparity_" + to_string(static_cast<long long>(jpeg_quality)) + ".bmp";
		Mat disparity_compressed(disparity.rows, disparity.cols, disparity.type());
		createDisparity(left_jpeg, right_jpeg, disparity_compressed);
		
		double error = norm(disparity, disparity_compressed, NORM_L2)/norm(disparity, NORM_L2);
		//double error = meanPercentageChange(disparity, disparity_compressed);
		//double error = correlationCoefficient(disparity, disparity_compressed);
		cout<<jpeg_quality<<'\t'<<error<<endl;
		imwrite(disparity_save_name, disparity_compressed);
	});
}







int main()
{
	bool createJpeg = 0;
	bool createDisparity = 1;
	bool compareDisparities = 0;

	//Mat img = imread("image.png", 0);

	//Paths for input & output
	string input_path = "E:\\DATA\\Pave 3D\\Sample Acqisition Images\\";	
	string output_path = "E:\\DATA\\Pave 3D\\disparity_images\\";

	if (createJpeg)
		createPavementJpegs(input_path, output_path);
	if (createDisparity)
		createDisparityFromJpegs(input_path, output_path);
	if (compareDisparities)
		compareDisparitiesCorrel(output_path);


	//Comparing intensities
	/*string left_image_name = input_path + "A_0000.bmp";
	string right_image_name = input_path + "B_0000.bmp";
	Mat left_image = imread(left_image_name, 0);
	Mat right_image = imread(right_image_name, 0);	
	compareIntensityCorrel(left_image, output_path, "left");
	compareIntensityCorrel(right_image, output_path, "right");*/


    return(0);
}