# include "FocusToolBackend.h"

FocusToolBackend::FocusToolBackend() {}

FocusToolBackend::FocusToolBackend(int r, int c)
{
	rows = r; cols = c;
	frameWindowName = "Camera Focus Tool";
	top_left_point = Point(0, 0);
	bottom_right_point = Point(0, 0);
	select_flag = 0;
	drag = 0;
	rect = Rect(rows, cols, CV_8UC3, 0);
	pattern = Mat(rows, cols, CV_8UC3, 0);
	focus_measure = 0.0;
}

FocusToolBackend::~FocusToolBackend() {}

/*******************STEP 2:Covert data from any space and format to opencv RGB mat********************/
void FocusToolBackend::ConvertToRGBMat(uchar *data, int transform_needed)
{
	switch (transform_needed)
	{
		case NO_TRANSFORM:
			frame = Mat(rows, cols, CV_8UC3, data);
			break;
		case UYVY_TO_RGB:
			frame = ConvertYUYToRGB(data);
		default:
			break;
	}
}

Mat FocusToolBackend::ConvertYUYToRGB(uchar *data)
{
	Mat YUYFrame(rows, cols, CV_8UC2, data);
	Mat RGBFrame = Mat(rows, cols, CV_8UC3, 0.0);
	cvtColor(YUYFrame , RGBFrame, CV_YUV2BGR_UYVY);
	return RGBFrame;
}
/******************************************************************************************************/





/********************STEP 3: Selects the ROI (focus pattern) on the current frame*********************/
void FocusToolBackend::PointClickSanityCheck(int *x, int *y, Mat image)
{
	if (*x >= image.cols) *x = image.cols-1;
	if(*x < 0) *x = 0;
	
	if(*y >= image.rows) *y = image.rows-1;	
	if(*y <0) *y = 0;
}

Rect FocusToolBackend::ExtractROI(Point *A, Point *B)
{
	if (A->x > B->x && A->y > B->y)
	{
		std::swap(B->x, A->x);
		std::swap(B->y, A->y);
	}

	if (A->x > B->x && A->y < B->y)
		std::swap(B->x, A->x);
		
	if (A->x < B->x && A->y > B->y)
		std::swap(B->y, A->y);

	return Rect(A->x, A->y, B->x-A->x,B->y-A->y);
}

void FocusToolBackend::MouseHandler(int event, int x, int y)
{
	PointClickSanityCheck(&x, &y, frame);

	//When button is clicked
	if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {       
        top_left_point = Point(x, y);
        drag = 1;
    }
    
	//When the button is clicked and dragged
    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
        Mat frame_temp = frame.clone();
        bottom_right_point = Point(x, y);
        rectangle(frame_temp, top_left_point, bottom_right_point, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow(frameWindowName, frame_temp);
    }
    
	//When the button is released
    if (event == CV_EVENT_LBUTTONUP && drag)
    {
        bottom_right_point = Point(x, y);	
		rect = ExtractROI(&top_left_point, &bottom_right_point);        
		drag = 0;
        pattern = frame(rect);
    }
    
	//Default case (do nothing)
    if (event == CV_EVENT_LBUTTONUP)
    {
        select_flag = 1;
        drag = 0;
    }
}

void FocusToolBackend::MouseHandler(int event, int x, int y, int flags, void* param)
{
	FocusToolBackend* ft = reinterpret_cast<FocusToolBackend*>(param);
	ft->MouseHandler(event, x, y);	
}
/********************************************************************************************************/

//STEP 4A: Computes edges for the selected ROI in the RGB image
void FocusToolBackend::ComputeEdgesForROI()
{
	//Compute Edges
	Mat edges;
	cvtColor(pattern, edges, CV_BGR2GRAY);
	GaussianBlur( edges, edges, Size(BlurWindow,BlurWindow), 1/6*BlurWindow );
    Canny(edges, edges, CannyWindow, CannyWindow*CannyRatio, BlurWindow);
	
	//Return frame as edge image
	Mat edge_layers[3] = {edges, edges, edges};
	merge(edge_layers, 3, pattern); 
}

//STEP 4B: Computes the focus measure for the selected ROI in the RGB image
double FocusToolBackend::ComputeFocusMeasureForROI()
{
	cv::Scalar mu, sigma;
    cv::meanStdDev(pattern, mu, sigma);
	return (sigma.val[0]*sigma.val[0]) / mu.val[0];	
}

//STEP 5: Overlay the computed Focus Score on the original frame
void FocusToolBackend::OverlayFocusMeasureAndDisplay()
{
	if (cvIsNaN(focus_measure)) focus_measure = 0;
	
	string text = "Focus Measure: " + std::to_string(focus_measure);
	CvFont font; 
	cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX_SMALL, 1, 1, 0.0, 1, 1);
	cv::Point textOrg(30,30);
	cv::putText(frame, text, textOrg, font.font_face, font.hscale, Scalar::all(0), 0.5, font.line_type, false );
	
	imshow(frameWindowName, frame);
    
}

//Main focus measure computation method
void FocusToolBackend::ProcessFocusMeasure(uchar *buffer, int transform_needed)
{	
	//Step 2: Conert to RGB Mat
	ConvertToRGBMat(buffer, transform_needed);

	//Step 3: Select the focus pattern region
	cvSetMouseCallback(frameWindowName.c_str(), MouseHandler, this);		

	//Step 4: Compute focus measure and display edges	
	if (select_flag == 1)
    {
		focus_measure = ComputeFocusMeasureForROI();			
		ComputeEdgesForROI();
    }
       		
	//Step 5: Overlay focus measure
	OverlayFocusMeasureAndDisplay();
}

int FocusToolBackend::tool() 
{
	return -1;
}