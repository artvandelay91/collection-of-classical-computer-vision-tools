#include "opencv\cv.h"
#include "opencv\highgui.h"

using namespace cv;

#define DLLExport __declspec(dllexport) 
#define NAME_CONV __cdecl

class DLLExport FocusToolBackend
{
	private:		
		int rows, cols;
		string frameWindowName;
		
		Point top_left_point, bottom_right_point;
		int drag, select_flag;
		Rect rect;
		Mat pattern;
		
		static const int BlurWindow = 3;
		static const int CannyWindow = 40;
		static const int CannyRatio = 3;

		double focus_measure;

		void ConvertToRGBMat(uchar *data, int transform_needed);
		Mat ConvertYUYToRGB(uchar *data);
		
		void PointClickSanityCheck(int *x, int *y, Mat image);
		Rect ExtractROI(Point *A, Point *B);
		void MouseHandler(int event, int x, int y);
		static void MouseHandler(int event, int x, int y, int flags, void* param);

		void ComputeEdgesForROI();
		
		double ComputeFocusMeasureForROI();
		
		void OverlayFocusMeasureAndDisplay();
	
	protected:
		void ProcessFocusMeasure(uchar *buffer, int transform_needed);

	public:		
		Mat frame;

		FocusToolBackend();
		FocusToolBackend(int rows, int cols);
		~FocusToolBackend();
		
		virtual int tool();

		enum COLOR_TRANSFORMS
		{
			CAPTURING_MAT = 0,
			NO_TRANSFORM = 1,
			UYVY_TO_RGB = 2
		};
};
