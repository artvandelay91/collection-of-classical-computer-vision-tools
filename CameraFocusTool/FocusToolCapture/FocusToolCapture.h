#include "FocusToolBackend.h"

#include <opencv\cv.h>
#include <opencv\highgui.h>

#include <atlbase.h> 
#include <DShow.h> 
#include <initguid.h>
#include <dvdmedia.h>
#include "SampleGrabber.h"

#include <iostream>

using namespace cv;

#define DLLExport __declspec(dllexport) 
#define NAME_CONV __cdecl

class FocusToolUSB : private FocusToolBackend
{
private:
		VideoCapture usb_cam;

	public:
		FocusToolUSB(int usb_cam_id, int rows, int cols) : FocusToolBackend(rows, cols) 
		{
			usb_cam = VideoCapture(usb_cam_id);
		}

		~FocusToolUSB();
		int tool();
};

DEFINE_GUID(CLSID_VideoCaptureSources,
0x860BB310, 0x5D01, 0x11D0, 0xBD, 0x3B, 0x00, 0xA0, 0xC9, 0x11, 0xCE, 0x86); //

DEFINE_GUID(CLSID_SampleGrabber,
0xC1F400A0, 0x3F08, 0x11D3, 0x9F, 0x0B, 0x00, 0x60, 0x08, 0x03, 0x9E, 0x37);

class FocusToolHD : private FocusToolBackend, public ISampleGrabberCB
{
private:

	//Methods to build and run the graph
	CComPtr<IGraphBuilder> graph;
	BOOL hrcheck(HRESULT hr, TCHAR* errtext);
	HRESULT CHECK_HR(HRESULT hr, TCHAR* msg);
	CComPtr<IBaseFilter> CreateFilterByName(const WCHAR* filterName, const GUID& category);
	CComPtr<IPin> GetPin(IBaseFilter *pFilter, LPCOLESTR pinname);
	HRESULT BuildGraph ();
	void RunGraph();

	//Methods inherited from ISampleGrabberCB
	STDMETHODIMP QueryInterface(REFIID riid, void **ppv) ;	
	STDMETHODIMP_(ULONG) AddRef(); 
	STDMETHODIMP_(ULONG) Release();
	STDMETHODIMP SampleCB(double SampleTime, IMediaSample *pSample);
	STDMETHODIMP BufferCB(double SampleTime, BYTE *pBuffer, long BufferLen);

public:
	
	FocusToolHD(int rows, int cols):FocusToolBackend(rows, cols)
	{
		CoInitialize(NULL);	
		graph.CoCreateInstance(CLSID_FilterGraph);
	}
	~FocusToolHD()
	{
		CoUninitialize();
	}
	int tool();
};


//DLL method
extern "C"
{
	DLLExport int NAME_CONV FocustoolUSB(int cam_id, int rows, int cols);
	DLLExport int NAME_CONV FocustoolHD(int rows, int cols);
}
