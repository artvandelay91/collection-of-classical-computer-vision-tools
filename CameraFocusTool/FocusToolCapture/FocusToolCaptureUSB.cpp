#include "FocusToolCapture.h"

FocusToolUSB::~FocusToolUSB(){}

//Focus tool for USB cameras
int FocusToolUSB::tool()
{
    if(!usb_cam.isOpened())
		return -1;
	
	//Process	
    while(true)
    {
		//Capture a frame
		usb_cam >> frame;
		
		//Compute focus measure
		ProcessFocusMeasure(frame.data, CAPTURING_MAT);
		
		//Display
		if(waitKey(30) >= 0) break;
    }

	//delete tool;
	return 0;
}

int FocustoolUSB(int usb_cam_id, int rows, int cols)
{
	FocusToolUSB FUSB (usb_cam_id, rows, cols);
	return FUSB.tool();
}
