#include "FocusToolCapture.h"

STDMETHODIMP FocusToolHD::QueryInterface(REFIID riid, void **ppv) 
{      
	if (NULL == ppv) return E_POINTER;
	if (riid == __uuidof(IUnknown)) {
		*ppv = static_cast<IUnknown*>(this);
			return S_OK;
	}
	if (riid == __uuidof(ISampleGrabberCB))   {
		*ppv = static_cast<ISampleGrabberCB*>(this);
			return S_OK;
	}
	return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG) FocusToolHD::AddRef() 
{    
	return S_OK;  
}

STDMETHODIMP_(ULONG) FocusToolHD::Release() 
{   
	return S_OK;  
}

STDMETHODIMP FocusToolHD::BufferCB(double SampleTime, BYTE *pBuffer, long BufferLen) 
{ 
	return S_OK; 
}

STDMETHODIMP FocusToolHD::SampleCB(double SampleTime, IMediaSample *pSample)
{
	if (!pSample)
		return E_POINTER;

	long sz = pSample->GetActualDataLength();
	BYTE *pBuf = NULL;
	pSample->GetPointer(&pBuf);
	if (sz <= 0 || pBuf==NULL) return E_UNEXPECTED;	
	ProcessFocusMeasure(static_cast<unsigned char*>(pBuf), UYVY_TO_RGB);
	if(waitKey(30) < 0) return S_OK;
}

BOOL FocusToolHD::hrcheck(HRESULT hr, TCHAR* errtext)
{
	if (hr >= S_OK)
	   return FALSE;
	TCHAR szErr [MAX_ERROR_TEXT_LEN];
	DWORD res = AMGetErrorText(hr, szErr, MAX_ERROR_TEXT_LEN);
	if (res)	
	   _tprintf(_T("Error %x: %s\n%s\n"), hr, errtext, szErr);
	else
	   _tprintf(_T("Error %x: %s\n"), hr, errtext);
	return TRUE;
}

HRESULT FocusToolHD::CHECK_HR(HRESULT hr, TCHAR* msg) 
{
	if (hrcheck(hr, msg)) 
		return hr;
}

CComPtr<IBaseFilter> FocusToolHD::CreateFilterByName(const WCHAR* filterName, const GUID& category)
{
	HRESULT hr = S_OK;
	CComPtr<ICreateDevEnum> pSysDevEnum;
	hr = pSysDevEnum.CoCreateInstance(CLSID_SystemDeviceEnum);
	if (hrcheck(hr, _T("Can�t create System Device Enumerator")))
	   return NULL;

	CComPtr<IEnumMoniker> pEnumCat;
	hr = pSysDevEnum->CreateClassEnumerator (category, &pEnumCat, 0);

	if (hr == S_OK)
	{
		CComPtr<IMoniker> pMoniker;
		ULONG cFetched;
		while (pEnumCat->Next(1, &pMoniker, &cFetched) == S_OK)
		{
			CComPtr<IPropertyBag> pPropBag;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
			if (SUCCEEDED(hr))
			{
				VARIANT varName;
				VariantInit(&varName);
				hr = pPropBag->Read(L"FriendlyName", &varName, 0);
				if (SUCCEEDED(hr))
				{
					if(wcscmp(filterName, varName.bstrVal) == 0){
						CComPtr<IBaseFilter> pFilter;
						hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void **)&pFilter);
						if (hrcheck(hr, _T("Can�t bind moniker to filter object")))
							return NULL;
						return pFilter;
					}
				}
				VariantClear(&varName);
			}
			pMoniker.Release();
		}
    	}
    	return NULL;
}

CComPtr<IPin> FocusToolHD::GetPin(IBaseFilter *pFilter, LPCOLESTR pinname)
{
	CComPtr<IEnumPins> pEnum;
	CComPtr<IPin> pPin;
	
	HRESULT hr = pFilter->EnumPins(&pEnum);
	if (hrcheck(hr, _T("Can�t enumerate pins.")))
		return NULL;
	
	while(pEnum->Next(1, &pPin, 0) == S_OK)
	{
		PIN_INFO pinfo;
		pPin->QueryPinInfo(&pinfo);
		BOOL found = !wcsicmp(pinname, pinfo.achName);
		if (pinfo.pFilter) pinfo.pFilter->Release();
		if (found)
			return pPin;
		pPin.Release();
	}
	printf("Pin not found! \n");
	return NULL;
}

HRESULT FocusToolHD::BuildGraph ()
{
	printf("Building Graph...\n");
	HRESULT hr = S_OK;
	
	//graph builder
	CComPtr<ICaptureGraphBuilder2> pBuilder;
	hr = pBuilder.CoCreateInstance(CLSID_CaptureGraphBuilder2);
	CHECK_HR(hr, _T("Can�t create Capture Graph Builder"));
	hr = pBuilder->SetFiltergraph(graph);
	CHECK_HR(hr, _T("Can�t SetFilterGraph"));

	//add Decklink Video Capture
	CComPtr<IBaseFilter> pDecklinkVideoCapture = CreateFilterByName(L"Decklink Video Capture", CLSID_VideoCaptureSources);
	hr = graph->AddFilter(pDecklinkVideoCapture, L"Decklink Video Capture");
	CHECK_HR(hr, _T("Can�t add Deckling Video Capture to graph"));

	//specify media type & format
	AM_MEDIA_TYPE pmt;
    ZeroMemory(&pmt, sizeof(AM_MEDIA_TYPE));
    pmt.majortype = MEDIATYPE_Video;
	//pmt.subtype = MEDIASUBTYPE_YUY2;
    pmt.formattype = FORMAT_VideoInfo;
    pmt.bFixedSizeSamples = TRUE;
    pmt.cbFormat = 104;
    pmt.lSampleSize = 4147200;
    pmt.bTemporalCompression = FALSE;  

	VIDEOINFOHEADER format;
    ZeroMemory(&format, sizeof(VIDEOINFOHEADER));
	format.dwBitRate = 994332712;
	format.AvgTimePerFrame = 333667;
	format.bmiHeader.biSize = 40;
    format.bmiHeader.biWidth = 1920;
    format.bmiHeader.biHeight = 1080;
    format.bmiHeader.biPlanes = 1;
    format.bmiHeader.biBitCount = 16;
    format.bmiHeader.biCompression = 1129923656; 
    format.bmiHeader.biSizeImage = 4147200;    
	pmt.pbFormat = (BYTE*)&format;

	//add sample grabber
	CComPtr<IBaseFilter> pSampleGrabber;
    hr = pSampleGrabber.CoCreateInstance(CLSID_SampleGrabber);
    CHECK_HR(hr, _T("Can't create SampleGrabber"));
    hr = graph->AddFilter(pSampleGrabber, L"SampleGrabber");
    CHECK_HR(hr, _T("Can't add SampleGrabber to graph"));
	CComQIPtr<ISampleGrabber, &IID_ISampleGrabber> isg(pSampleGrabber);
    hr = isg->SetMediaType(&pmt);
    CHECK_HR(hr, _T("Can't set Media Type to sample grabber"));    

	//callback to focus tool
	hr = isg->SetCallback(this, 0);
	CHECK_HR(hr, _T("Can't render stream to SampleGrabber"));

	////add AVI Decompressor
	//CComPtr<IBaseFilter> pAVIDecompressor;
	//hr = pAVIDecompressor.CoCreateInstance(CLSID_AVIDec);
	//CHECK_HR(hr, _T("Can�t create AVI Decompressor"));
	//hr = graph->AddFilter(pAVIDecompressor,L"AVI Decompressor");
	//CHECK_HR(hr, _T("Can�t add AVI Decompressor to graph"));
	//
	////add Video Renderer
	//CComPtr<IBaseFilter> pVideoRenderer;
	//hr = pVideoRenderer.CoCreateInstance(CLSID_VideoRenderer);
	//CHECK_HR(hr, _T("Can�t create Video Renderer"));
	//hr = graph->AddFilter(pVideoRenderer, L"Video Renderer");
	//CHECK_HR(hr, _T("Can�t add Video Renderer to graph"));

	
	//connect Decklink Video Capture and Sample Grabber
	hr = graph->ConnectDirect(GetPin(pDecklinkVideoCapture, L"Capture"), GetPin(pSampleGrabber, L"Input"), NULL);
	CHECK_HR(hr, _T("Can't connect Decklink Video Capture and Sample Grabber"));

	//connect SampleGrabber and AVI Decompressor
	/*hr = graph->ConnectDirect(GetPin(pSampleGrabber, L"Output"), GetPin(pAVIDecompressor, L"XForm In"), NULL);
	CHECK_HR(hr, _T("Can�t connect SampleGrabber and AVI Decompressor"));*/

	////connect AVI Decompressor and Video Renderer
	//hr = graph->ConnectDirect(GetPin(pAVIDecompressor, L"XForm Out"), GetPin(pVideoRenderer, L"VMR Input0"), NULL);
	//CHECK_HR(hr, _T("Can�t connect AVI Decompressor and Video Renderer"));

	return S_OK;
}

void FocusToolHD::RunGraph()
{
	printf("Running Graph...\n");
	
	CComQIPtr<IMediaControl, &IID_IMediaControl> mediaControl(graph);
	HRESULT hr = mediaControl->Run();
	CHECK_HR(hr, _T("Can�t run the graph"));
	
	CComQIPtr<IMediaEvent, &IID_IMediaEvent> mediaEvent(graph);
	BOOL stop = FALSE;
	MSG msg;
	
	while (!stop)
	{
		long ev=0;
		LONG_PTR p1=0, p2=0;
		Sleep(500);
		while(PeekMessage(&msg, NULL, 0 ,0, PM_REMOVE))
			DispatchMessage(&msg);
		while(mediaEvent->GetEvent(&ev, &p1, &p2, 0) == S_OK)
		{
			if (ev == EC_USERABORT)
			{
				printf("Done! \n");
				stop = TRUE;
			}
			else
			if(ev == EC_ERRORABORT)
			{
				printf("An error occurred: RESULT = %x\n", p1);
				mediaControl->Stop();
				stop = TRUE;
			}
			mediaEvent->FreeEventParams(ev, p1, p2);
		}
	}
}

int FocusToolHD::tool()
{
	//Construct Graph
	if (BuildGraph() != S_OK)
		return -1;

	//Run Graph
	RunGraph();

	return 0;
}

int FocustoolHD(int rows, int cols)
{	
	FocusToolHD FHD(rows, cols);		
	return FHD.tool();
} 