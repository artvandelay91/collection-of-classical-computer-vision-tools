﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace TestApplication
{
    class application
    {
        [DllImport("FocusToolCapture.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int FocustoolUSB(int usb_camera_id, int rows, int cols);

        [DllImport("FocusToolCapture.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int FocustoolHD(int rows, int cols);

        static void Main(string[] args)
        {

            RunFocusTool();
        }       

        private static void RunFocusTool()
        {
            //FocustoolUSB(0, 640, 480);
            FocustoolHD(1080, 1920);
        }
    }
}
