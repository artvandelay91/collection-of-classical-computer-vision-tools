#include "opencv2\core\core.hpp"
#include "opencv2\nonfree\features2d.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\stitching\stitcher.hpp"
#include "opencv2\features2d\features2d.hpp"
#include "opencv2\calib3d\calib3d.hpp"
#include "PanoCommon.h"
#include <iostream>

using namespace PanoConfiguration;
using namespace cv;

typedef struct PanoElement PanoStitchingElement;
typedef FileStatus StitchingStatus;

#pragma region STITCHER ELEMENT STRUCT
struct StitcherElement
{
	StitchSide _side;
	double _tx, _ty;
	Mat _recomputedH;
	Size _canvasSize;
	Point2f _refTlCorner;
	vector<Point2f> _projectedCorners;

	StitcherElement()
	{
		_side = StitchSide::HEAD;
		_tx = 0; _ty = 0;
		_recomputedH = Mat::eye(3, 3, CV_64FC1);
		_canvasSize = Size();
		_refTlCorner = Point2f();
		_projectedCorners = vector<Point2f>(4);
	}
};

typedef struct StitcherElement StitcherElement;
#pragma endregion

#pragma region PANO STITCH CLASS
class PanoStitch
{
	private:

		#pragma region VARIABLES
		int _nStreams;
		int _mImagesInStream;
		int _centerCameraIndex;

		vector<string> _camNames;
		vector<int> _camIds;

		vector<Mat> _homographies;

		vector<StitcherElement> _stitcherData;
		bool _computedStitcherData;

		vector<PanoStitchingElement> _stitchingData;	

		string _exceptionMessage;
		ProcessResult _result;
		#pragma endregion
		
		#pragma region METHODS

		#pragma region INITIALIZATION METHODS
		void LoadCalibrationFile(char *calibrationFile);
		bool ValidateLoadedCalibration(vector<string> camNames, vector<int> camIds);
		void LoadStitchingData(int nStreams, int mImagesInStream, char **streamNames, char **panoNames);
		#pragma endregion

		#pragma region PIPELINE METHODS
		void StitchImages();
		void CopyExceptionMessage(char *exceptionMessage, int *exceptionMessageLength);
		void CropPanorama(Mat &panoImage, Size orgSize);
		void CompensateExposure(Mat &panoImage, Size orgSize);
		#pragma endregion

		#pragma region STITCHING METHODS
		void RescaleHomgraphies();
		void PreprocesImages(vector<Mat> &streams);
		void StitchImagesRow(PanoStitchingElement &element, vector<Mat> streams, Mat &panoImage);
		void ComputeStitcherData(vector<Mat> streams);
		void ComputeStitcherInfo(int streamIdx, Size srcSize, int homographyId);
		void IdentifyFrameIdsForStitching(int j, vector<Mat> streams, Mat pano, Mat &refImage, Mat &srcImage);
		void StitchConsecutiveImages(Mat refImage, Mat srcImage, StitcherElement &sElement,  Mat &panoImage);
		ElementStatus SavePanorama(PanoStitchingElement &element, Mat panoImage);
		#pragma endregion
		
		#pragma endregion

		int StitchPanoImages(char *exceptionMessage, int *exceptionMessageLength);

	public:

		#pragma region CONSTRUCTOR METHODS
		PanoStitch();
		PanoStitch(int nStreams, int mImagesInStream, char **streamNames, char **panoNames, char *calibrationFile, char **camNames, int *camIds);
		~PanoStitch();
		#pragma endregion

		int StitchImagesBatch(char *exceptionMessage, int *exceptionMessageLength);
};
#pragma endregion

#pragma region DLL METHODS
#ifdef _BUILD_DLL_
    #define IMPORT_EXPORT __declspec(dllexport) 
	#define NAME_CONV __cdecl	
#else
    #define IMPORT_EXPORT __declspec(dllimport)
	#define NAME_CONV 
#endif


extern "C"
{
	IMPORT_EXPORT int NAME_CONV StitchImagesBatch(	int nStreams, int mImagesInStream, char **streamNames, char **camNames, int *camIds, 
													char *calibrationFile, char **panoNames, char exceptionMessage[], int *exceptionMessageLength	);
}
#pragma endregion