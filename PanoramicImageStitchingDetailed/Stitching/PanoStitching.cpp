#include "PanoStitching.h"

int StitchImagesBatch(	int nStreams, int mImagesInStream, char **streamNames, char **camNames, int *camIds, 
						char *calibrationFile, char **panoNames, char exceptionMessage[], int *exceptionMessageLength	)
{
	PanoStitch stitcher(nStreams, mImagesInStream, streamNames, panoNames, calibrationFile, camNames, camIds);
	return stitcher.StitchImagesBatch(exceptionMessage, exceptionMessageLength);
}

int PanoStitch::StitchImagesBatch(char *exceptionMessage, int *exceptionMessageLength)
{
	return StitchPanoImages(exceptionMessage, exceptionMessageLength);
}

int PanoStitch::StitchPanoImages(char *exceptionMessage, int *exceptionMessageLength)
{
	if (_result != ProcessResult::PROCESS_FAILURE)
		StitchImages();
	
	CopyExceptionMessage(exceptionMessage, exceptionMessageLength);
	return _result;
}