#include "PanoStitching.h"

void PanoStitch::RescaleHomgraphies()
{
	Mat scaleMat = Mat::eye(3, 3, CV_64FC1);
	scaleMat.at<double>(0,0) = 1/DOWNLSAMPLE_FACTOR;
	scaleMat.at<double>(1,1) = 1/DOWNLSAMPLE_FACTOR;

	for(int i=0; i<_homographies.size(); i++)
		_homographies[i] = scaleMat * _homographies[i];
}

void PanoStitch::PreprocesImages(vector<Mat> &streams)
{
	for(int i=0; i<streams.size(); i++)
		resize(streams[i], streams[i], Size(), 1/DOWNLSAMPLE_FACTOR, 1/DOWNLSAMPLE_FACTOR);
}

void PanoStitch::StitchImagesRow(PanoStitchingElement &element, vector<Mat> streams, Mat &panoImage)
{		
	if(!_computedStitcherData) ComputeStitcherData(streams);
	
	int homographyId = 0;

	for (int streamIdx=0; streamIdx < _nStreams; streamIdx++)
	{	
		if(streamIdx == _centerCameraIndex) continue;
			
		Mat refImage, srcImage;
		IdentifyFrameIdsForStitching(streamIdx, streams, panoImage, refImage, srcImage);

		StitchConsecutiveImages(refImage, srcImage, _stitcherData[homographyId] , panoImage);
		/*if(streamIdx<_centerCameraIndex)
			imwrite("Left-CenterStitched.jpg", panoImage);
		else
			imwrite("Left-Center-RightStitched.jpg", panoImage);*/
		//blender.prepare(Rect(0, 0, panoImage.cols, panoImage.rows));

		homographyId++;		
	}
	
}

Mat makeMask(Size sz, vector<Point2f> imageCorners, Mat homorgaphy) {
  Scalar white(255, 255, 255);
  Mat mask = Mat::zeros(sz, CV_8U);
  Point2f innerPoint;
  vector<Point2f> transformedCorners(4);

  perspectiveTransform(imageCorners, transformedCorners, homorgaphy);
  // Calculate inner point
  for (auto& point : transformedCorners)
      innerPoint += point;
  innerPoint.x /= 4;
  innerPoint.y /= 4;

  // Make indent for each corner
  vector<Point> corners;
  for (int ind = 0; ind < 4; ++ind) {
    Point2f direction = innerPoint - transformedCorners[ind];
    double normOfDirection = norm(direction);
    corners[ind].x += 1 * direction.x / normOfDirection;
    corners[ind].y += 1 * direction.y / normOfDirection;
  }

  // Draw borders
  Point prevPoint = corners[3];
  for (auto& point : corners) {
    line(mask, prevPoint, point, white);
    prevPoint = point;
  }

  // Fill with white
  floodFill(mask, innerPoint, white);
  return mask;
}


void PanoStitch::ComputeStitcherData(vector<Mat> streams)
{
	int stitchIdx = 0;
	for (int streamIdx=0; streamIdx < _nStreams; streamIdx++)
	{	
		if(streamIdx == _centerCameraIndex) continue;

		ComputeStitcherInfo(streamIdx, streams[streamIdx].size(), stitchIdx);
		
		stitchIdx++;
	}
	_computedStitcherData = true;
}

void PanoStitch::ComputeStitcherInfo(int streamIdx, Size srcSize, int stitchIdx)
{	
	_stitcherData[stitchIdx]._side = (streamIdx < _centerCameraIndex) ?  StitchSide::LEFT : StitchSide::RIGHT;

	vector<Point2f> src(4), dst(4);
	src[0] = Point2f(0, 0);
	src[1] = Point2f(srcSize.width, 0);
	src[2] = Point2f(srcSize.width, srcSize.height);
	src[3] = Point2f(0, srcSize.height);	
	perspectiveTransform(src, dst, _homographies[stitchIdx]);

	if(_stitcherData[stitchIdx]._side == StitchSide::LEFT)
	{
		_stitcherData[stitchIdx]._tx = -min(dst[0].x, dst[3].x);
		_stitcherData[stitchIdx]._ty = -min(dst[0].y, dst[1].y);
	}
	else
	{
		_stitcherData[stitchIdx]._tx = _stitcherData[stitchIdx - 1]._tx;
		_stitcherData[stitchIdx]._ty = _stitcherData[stitchIdx - 1]._ty;
	}

	Mat newH = Mat::eye(3, 3, _homographies[stitchIdx].type());
	newH.at<double>(0, 2) = _stitcherData[stitchIdx]._tx;
	newH.at<double>(1, 2) = _stitcherData[stitchIdx]._ty;
	_stitcherData[stitchIdx]._recomputedH = newH * _homographies[stitchIdx];

	perspectiveTransform(src, _stitcherData[stitchIdx]._projectedCorners, _stitcherData[stitchIdx]._recomputedH);

	double canvasWidth = 0.0, canvasHeight = 0.0;
	if(_stitcherData[stitchIdx]._side == StitchSide::LEFT)
	{
		canvasWidth = _stitcherData[stitchIdx]._tx + srcSize.width;
		canvasHeight = dst[3].y - dst[0].y;
	}
	else
	{
		canvasWidth = (max(_stitcherData[stitchIdx]._projectedCorners[1].x, _stitcherData[stitchIdx]._projectedCorners[2].x) - min(_stitcherData[stitchIdx]._projectedCorners[0].x, _stitcherData[stitchIdx]._projectedCorners[3].x));
		canvasWidth += _stitcherData[stitchIdx]._projectedCorners[0].x;
		canvasHeight = max((float)_stitcherData[stitchIdx - 1]._canvasSize.height, (dst[3].y - dst[1].y));
	}
	_stitcherData[stitchIdx]._canvasSize = Size(canvasWidth, canvasHeight);

	double refStartX = 0.0, refStartY = 0.0;
	if(_stitcherData[stitchIdx]._side == StitchSide::LEFT)
	{
		refStartX = _stitcherData[stitchIdx]._tx;
		refStartY = _stitcherData[stitchIdx]._ty;
	}
	_stitcherData[stitchIdx]._refTlCorner = Point2f(refStartX, refStartY);
}

void PanoStitch::IdentifyFrameIdsForStitching(int streamIdx, vector<Mat> streams, Mat pano, Mat &refImage, Mat &srcImage)
{
	if(streamIdx < _centerCameraIndex)
	{
		refImage = streams[streamIdx+1];
		if (pano.rows == 0)
			srcImage = streams[streamIdx];
		else
			srcImage = pano;
	}
	else
	{
		srcImage = streams[streamIdx];
		if(pano.rows==0)
			refImage = streams[streamIdx-1];
		else
			refImage = pano;
	}
}

void PanoStitch::StitchConsecutiveImages(Mat refImage, Mat srcImage, StitcherElement &sElement, Mat &result)
{		
	warpPerspective(srcImage, result, sElement._recomputedH, sElement._canvasSize);
	refImage.copyTo(result(Rect(sElement._refTlCorner.x, sElement._refTlCorner.y, refImage.cols, refImage.rows)));

	//namedWindow("Result", CV_WINDOW_KEEPRATIO);
	//imshow( "Result", result );
	//waitKey(0);
}

ElementStatus PanoStitch::SavePanorama(PanoStitchingElement &element, Mat panoImage)
{	
	if (!imwrite(element.panoName, panoImage))
		return ElementStatus::SAVE_ERROR;

	return ElementStatus::SAVED_PANORAMA;
}