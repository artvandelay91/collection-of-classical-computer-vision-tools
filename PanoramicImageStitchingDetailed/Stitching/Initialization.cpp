#include "PanoStitching.h"

PanoStitch::PanoStitch()
{
	
}

PanoStitch::PanoStitch(int nStreams, int mImagesInStream, char **streamNames, char **panoNames, char *calibrationFile, char **camNames, int *camIds)
{
	_result = ProcessResult::PROCESS_NOT_STARTED;
	_nStreams = nStreams;
	_mImagesInStream = mImagesInStream;
	_exceptionMessage = "";
	for(int i=0; i<_nStreams; i++)
	{
		_camNames.push_back(string(camNames[i]));
		_camIds.push_back(camIds[i]);
	}
	_computedStitcherData = false;

	LoadCalibrationFile(calibrationFile);
	if(_result!=ProcessResult::PROCESS_FAILURE)
		LoadStitchingData(nStreams, mImagesInStream, streamNames, panoNames);

	for(int i=0; i<_nStreams; i++)
	{
		if (i == _centerCameraIndex) continue;
			_stitcherData.push_back(StitcherElement());
	}
}

void PanoStitch::LoadCalibrationFile(char *calibrationFile)
{
	
	FileStorage fs;
	fs.open(calibrationFile, FileStorage::READ);
	if(!fs.isOpened())
	{
		_result = ProcessResult::PROCESS_FAILURE;
		_exceptionMessage = "Unable to open the calibration file.";
		return;
	}

	FileNode node = fs["HEADER"];
	if (node.type() == FileNode::NONE || node["CENTER_CAMERA_INDEX"].type() == FileNode::NONE ||
		node["STREAM_NAMES"].type() != FileNode::SEQ || 
		node["CAMERA_IDS"].type() != FileNode::SEQ)
    {
		_result = ProcessResult::PROCESS_FAILURE;
		_exceptionMessage = "Error reading the calibration file. Corrupt header info.";
		return;
    }

	node["CENTER_CAMERA_INDEX"] >> _centerCameraIndex;

	FileNodeIterator it = node["STREAM_NAMES"].begin(), it_end = node["STREAM_NAMES"].end();
	vector<string> camNames;
	for (; it != it_end; ++it)
		camNames.push_back((string)*it);

	it = node["CAMERA_IDS"].begin(), it_end = node["CAMERA_IDS"].end();
	vector<int> camIds;
	for (; it != it_end; ++it)
		camIds.push_back((int)*it);

	if(!ValidateLoadedCalibration(camNames, camIds))
	{
		_result = ProcessResult::PROCESS_FAILURE;
		return;
	}

	for(int i=0; i<_nStreams-1; i++)
	{
		string name = _camNames[i] + "_" + _camNames[i+1];
		Mat H; fs[name] >> H;
		_homographies.push_back(H);
	}
}

bool PanoStitch::ValidateLoadedCalibration(vector<string> camNames, vector<int> camIds)
{
	if(camNames.size() != _camNames.size() || camIds.size() != _camIds.size())
	{		
		_exceptionMessage = "Number of image streams do not match";
		return false;
	}

	for(int i=0; i<_nStreams; i++)
	{
		if (camNames[i] != _camNames[i] || camIds[i] != camIds[i])
		{
			_exceptionMessage = "Camera names and ids used does match the ones in the file";
			return false;
		}
	}
	return true;
}

void PanoStitch::LoadStitchingData(int nStreams, int mImagesInStream, char **streamNames, char **panoNames)
{
	for(int i=0, j=0; i<nStreams*mImagesInStream && j<mImagesInStream; i+=nStreams, j++)
	{
		vector<string> streamnames;

		for(int j=0; j<3; j++)
			streamnames.push_back(string(streamNames[i+j]));
		
		_stitchingData.push_back(PanoStitchingElement(streamnames, string(panoNames[j]), ""));

	}
}

PanoStitch::~PanoStitch()
{

}