#include "PanoStitching.h"
static bool windowsPositioned = false;
void PanoStitch::StitchImages()
{
	//RescaleHomgraphies();
	
	for(int i=0; i<_mImagesInStream; i++)
	{		
		//Read the images
		vector<Mat> streams;
		if (ReadImages(_stitchingData[i], streams, _nStreams, false) != ElementStatus::LOADED_IMAGES)
		{
			BuildExceptionMessage(0, _stitchingData[i], StitchingStatus::CORRUPT_IMAGES);
			continue;
		}



		//Preprocess images
		//PreprocesImages(streams);

		//Stitch a row of images
		Mat panoImage;
		Stitcher stitcher = Stitcher::createDefault();
		Stitcher::Status status = stitcher.stitch(streams, panoImage);

		//StitchImagesRow(_stitchingData[i], streams, panoImage);

		//Crop out the black regions
		//CropPanorama(panoImage, streams[0].size());
		//imwrite("panorama.jpg", panoImage);
		
		//Exposure compensation
		//CompensateExposure(panoImage, streams[0].size());
		//imwrite("exposurecompensated.jpg", panoImage);
		//if(!windowsPositioned) { waitKey(0); windowsPositioned = true;}	
		//else if(waitKey(30) >= 0) return;
		//Save the stitched image
		 if (Stitcher::OK == status) 
		 {
			if (SavePanorama(_stitchingData[i], panoImage) != ElementStatus::SAVED_PANORAMA)
				BuildExceptionMessage(0, _stitchingData[i], StitchingStatus::WRITE_ERROR);
		 }
		//cout<<i<<endl;
	}
}

void PanoStitch::CopyExceptionMessage(char *exceptionMessage, int *exceptionMessageLength)
{
	*exceptionMessageLength = _exceptionMessage.size();
	memcpy(exceptionMessage, _exceptionMessage.c_str(), *exceptionMessageLength*sizeof(char));
}