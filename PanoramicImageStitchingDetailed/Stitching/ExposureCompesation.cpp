#include "PanoStitching.h"

void PanoStitch::CropPanorama(Mat &panoImage, Size orgSize)
{
	//double startPoint = max(_stitcherData[0]._projectedCorners[0].x, _stitcherData[0]._projectedCorners[3].x);
	//double endPoint = min(_stitcherData[1]._projectedCorners[1].x, _stitcherData[1]._projectedCorners[2].x);
	//
	//double centerX = _stitcherData[0]._tx + orgSize.width/2, centerY = _stitcherData[0]._ty + orgSize.height/2;

	//Rect cropZone(startPoint, (centerY - (orgSize.height/2)), (endPoint - startPoint), orgSize.height);
	Rect cropZone(100, 300, panoImage.cols-100, panoImage.rows-600);
	panoImage = panoImage(cropZone);
}

void Clahe(Mat &image)
{
	

}

void PanoStitch::CompensateExposure(Mat &panoImage, Size orgSize)
{
	cv::Mat lab_image;
	cv::cvtColor(panoImage, lab_image, CV_BGR2Lab);
	// Extract the L channel
	std::vector<cv::Mat> lab_planes(3);
	cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

	// apply the CLAHE algorithm to the L channel
	cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	clahe->setClipLimit(4);
	cv::Mat dst;
	clahe->apply(lab_planes[0], dst);

	// Merge the the color planes back into an Lab image
	dst.copyTo(lab_planes[0]);
	cv::merge(lab_planes, lab_image);

	// convert back to RGB
	cv::Mat image_clahe;
	cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);

	panoImage = image_clahe;
}
