#include <string>
#include <vector>
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\stitching\stitcher.hpp"
#include "Config.h"

using namespace std;
using namespace cv;

#pragma region PANO CALIBRATION ELEMENT STRUCT
struct PanoElement
{
	vector<string> streamNames;
	string panoName;
	string exceptionMessage; 

	PanoElement()
	{
	
	}
	
	PanoElement(vector<string> streamnames, string panoname, string exceptionmessage)
	{
		streamNames = streamnames;
		panoName = panoname;
		exceptionMessage = exceptionmessage;
	}

	~PanoElement()
	{
		vector<string>().swap(streamNames);
	}
};
typedef struct PanoElement PanoElement;
#pragma endregion


#pragma region PANO METADATA
struct PanoMetaData
{
	vector<string> _camNames;
	vector<int>_camIds;	
	string _clientName;
	string _dbName;
	
	PanoMetaData()
	{
		
	}

	PanoMetaData(int nStreams, char *clientName, char *dbName, char **camNames, int *camIds)
	{
		_clientName = string(clientName);
		_dbName = string(dbName);
		for(int i=0; i<nStreams; i++)
		{
			_camNames.push_back(camNames[i]);
			_camIds.push_back(camIds[i]);
		}
	}

	~PanoMetaData()
	{
		vector<string>().swap(_camNames);
		vector<int>().swap(_camIds);
	}
};
typedef struct PanoMetaData PanoMetaData;
#pragma endregion

#ifdef _BUILD_DLL_
    #define IMPORT_EXPORT __declspec(dllexport) 
	#define NAME_CONV __cdecl	
#else
    #define IMPORT_EXPORT __declspec(dllimport)
	#define NAME_CONV 
#endif

#pragma region DLL METHODS
extern "C"
{
	IMPORT_EXPORT PanoConfiguration::ElementStatus NAME_CONV ReadImages(PanoElement &element, vector<Mat> &streams, int nStreams, bool isGray);
	IMPORT_EXPORT void NAME_CONV BuildExceptionMessage(int homographyId, PanoElement &pElement, PanoConfiguration::FileStatus status);
	IMPORT_EXPORT void NAME_CONV ShowImage(string winName, Mat image, int waitkey);
	IMPORT_EXPORT void NAME_CONV PlotPoints(Mat &image, vector<Point2f> points);
	IMPORT_EXPORT void NAME_CONV DisplayPlottedImage(Mat refImage, Mat srcImage, vector<Point2f> refPts, vector<Point2f> srcPts, string prefix);
	IMPORT_EXPORT Mat NAME_CONV DrawFeatureMatches(Mat refImage, Mat srcImage, vector<KeyPoint> keypointsRef, vector<KeyPoint> keypointsSrc, vector<DMatch> goodMatches);
}
#pragma endregion