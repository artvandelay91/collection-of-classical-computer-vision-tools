#include "PanoCommon.h"
#include <iostream>

PanoConfiguration::ElementStatus ReadImages(PanoElement &element, vector<Mat> &streams, int nStreams, bool isGray)
{
	for(int i=0; i<nStreams; i++)
	{
		Mat img;

		unsigned int loadOption = (isGray) ? CV_LOAD_IMAGE_GRAYSCALE : CV_LOAD_IMAGE_COLOR;
		img = imread(element.streamNames[i], loadOption);

		if(!img.data)	
			return PanoConfiguration::ElementStatus::CORRUPT_IMAGE;

		streams.push_back(img);
	}
	return PanoConfiguration::ElementStatus::LOADED_IMAGES;
}

void BuildExceptionMessage(int homographyId, PanoElement &pElement, PanoConfiguration::FileStatus status)
{
	if (status == PanoConfiguration::FileStatus::SUCCESS) return;
	 
	switch (status)
	{
		case PanoConfiguration::FileStatus::NO_GOOD_MATCH:
			pElement.exceptionMessage = "Not enough corresponding features between ";
			break;

		case PanoConfiguration::FileStatus::ERROR_COMPUTING_HOMOGRAPHY:
			pElement.exceptionMessage = "Not enough inliers in homography computation between ";
			break;

		case PanoConfiguration::FileStatus::CONVERGENCE_ERROR:
			pElement.exceptionMessage = "Homography converges to NaN between  ";
			break;
		
		case PanoConfiguration::FileStatus::CORRUPT_IMAGES:
			pElement.exceptionMessage = "One or all of the following images not found or corrupt";
			for(int i=0; i<pElement.streamNames.size(); i++)
				pElement.exceptionMessage += ", " + pElement.streamNames[i];
			return;
		case PanoConfiguration::FileStatus::WRITE_ERROR:
			pElement.exceptionMessage = "Output path does not exist";
			return;
		default:
			break;
	}
		
	pElement.exceptionMessage += pElement.streamNames[homographyId] + " and " + pElement.streamNames[homographyId+1]; 
}

void  ShowImage(string winName, Mat image, int waitkey)
{
	namedWindow(winName, CV_WINDOW_KEEPRATIO);
	imshow(winName, image);
}

void PlotPoints(Mat &image, vector<Point2f> points)
{
	for(int i=0; i<points.size(); i++)
		circle(image, points[i], 5, Scalar(255, 0, 0), 2, 4);
}

void DisplayPlottedImage(Mat refImage, Mat srcImage, vector<Point2f> refPts, vector<Point2f> srcPts, string prefix)
{
	PlotPoints(refImage, refPts);
	PlotPoints(srcImage, srcPts);
	string refName = prefix + " ref";
	string srcName = prefix + " src";
	ShowImage(refName, refImage, -1);
	ShowImage(srcName, srcImage, -1);
}

Mat DrawFeatureMatches(Mat refImage, Mat srcImage, vector<KeyPoint> keypointsRef, vector<KeyPoint> keypointsSrc, vector<DMatch> goodMatches)
{
	Mat imgMatches;
	drawMatches(	refImage, keypointsRef, srcImage, keypointsSrc,
					goodMatches, imgMatches, Scalar::all(-1), Scalar::all(-1),
					vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS	);
	return imgMatches;
	////-- Show detected matches
	//ShowImage("matches", imgMatches, 1);
}