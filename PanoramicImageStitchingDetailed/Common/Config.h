using namespace std;

namespace PanoConfiguration
{
	#pragma region CONSTANTS
	const int MIN_GOOD_IMAGES_NEEDED = 5;
	const int MIN_IMAGES_CONVERGANCE = 10;
	const int MIN_GOOD_FEATURES = 20;
	const int MIN_RANSAC_INLIERS = 12;
	const int MIN_DIST_FACTOR = 3;
	const double MAX_CONVERGANCE_ERROR = 0.5;
	const double DOWNLSAMPLE_FACTOR = 4;
	const double SEARCH_AREA_PERCENTAGE = 0.7;
	const double MAX_REPROJECTION_ERROR = 1;
	const double KNN_MATCH_RATIO = 0.8;
	const string COMPANY_NAME = "FUGRO ROADWARE";
	#pragma endregion

	enum FileStatus
	{
		NOT_PROCESSED = 0,
		CORRUPT_IMAGES = -1,
		NO_GOOD_MATCH = -2,
		GOOD_MATCHES_FOUND = 2,
		ERROR_COMPUTING_HOMOGRAPHY = -3,
		HOMOGRAPHY_COMPUTED = 3,
		CONVERGENCE_ERROR = -4,
		CONVERGENCE_CHECKED = 4,
		WRITE_ERROR = -5,
		SUCCESS = 1
	};

	#pragma region ERROR CODES
	enum StitchSide
	{
		LEFT = -1,
		RIGHT = 1,
		HEAD = 0
	};

	enum ProcessResult
	{
		PROCESS_NOT_STARTED = 0,
		PROCESS_SUCCESS = 1,
		PROCESS_COMPLETED_WITH_WARNINGS = -2,
		PROCESS_FAILURE = -1
	};

	enum ElementStatus
	{
		CORRUPT_IMAGE = -2,
		LOADED_IMAGES = 2,
		SAVE_ERROR = -3,
		SAVED_PANORAMA = 3
	};
	#pragma endregion
}