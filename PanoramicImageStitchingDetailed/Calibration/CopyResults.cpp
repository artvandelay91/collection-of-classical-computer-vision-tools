#include "PanoCalibration.h"

void ComputeHomography::CopyHomographies(double *stitchingParameters)
{
	int copyIdx = 0;
	for(int i=0; i<_homographies.size(); i++)
	{
		int length = _homographies[i]._h.total();
		memcpy(stitchingParameters + copyIdx, _homographies[i]._h.data, length*sizeof(double));
		copyIdx += length;
	}	
}

void ComputeHomography::CopyExceptionMessage(char *exceptionMessage, int *exceptionMessageLength)
{
	*exceptionMessageLength = _exceptionMessage.size();
	memcpy(exceptionMessage, _exceptionMessage.c_str(), *exceptionMessageLength*sizeof(char));
}