#include "PanoCalibration.h"

void ComputeHomography::ComputeStatus()
{
	if (!_allConverged)
	{
		int minGoodImageCount = INT16_MAX;
		for (int i=0; i<_homographies.size(); i++)
		{
			if(_homographies[i]._goodImagesCount < minGoodImageCount)
				minGoodImageCount = _homographies[i]._goodImagesCount;
		}

		if(minGoodImageCount < MIN_GOOD_IMAGES_NEEDED)
			_result = ProcessResult::PROCESS_FAILURE;
		else
			_result = ProcessResult::PROCESS_COMPLETED_WITH_WARNINGS;
	}
	else
	{
		_result = ProcessResult::PROCESS_SUCCESS;
	}
}

void ComputeHomography::CreateExceptionMessage()
{
	for(int i=0; i<_calibrationData.size(); i++)
	{
		if(_calibrationData[i].exceptionMessage == "") continue;

		_exceptionMessage += _calibrationData[i].exceptionMessage + "\n\n";
	}
}