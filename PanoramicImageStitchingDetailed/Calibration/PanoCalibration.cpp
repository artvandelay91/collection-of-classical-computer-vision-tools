#include "PanoCalibration.h"

int ComputeStitchingParameters(	int nStreams, int nImagesInStream, int centerCamIndex, char **streamNames, double stitchingParameters[], 
								char *clientName, char *dbName, char **camNames, int *camIds, char *outputLocation, 
								char exceptionMessage[],int *exceptionMessageLength	)
{
	ComputeHomography computer(nStreams, nImagesInStream, centerCamIndex, streamNames, clientName, dbName, camNames, camIds, outputLocation);
	return computer.ComputeStitchingParameters(stitchingParameters, exceptionMessage, exceptionMessageLength);
}

int ComputeHomography::ComputeStitchingParameters(double *stitchingParameters, char *exceptionMessage, int *exceptionMessageLength)
{
	return ComputeStitchingHomographies(stitchingParameters, exceptionMessage, exceptionMessageLength);
}

int ComputeHomography::ComputeStitchingHomographies(double *stitchingParameters, char *exceptionMessage, int *exceptionMessageLength)
{
	//Compute the stitching parameters
	ComputeHomographies();	

	//Write stitching parameters to file
	WriteParametersToFile();

	//Compute Calibration status
	ComputeCalibrationStatus();

	//Copy results
	CopyResults(stitchingParameters, exceptionMessage, exceptionMessageLength);
	
	return _result;
}