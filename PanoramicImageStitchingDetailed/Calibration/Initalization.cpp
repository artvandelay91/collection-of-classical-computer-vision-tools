#include "PanoCalibration.h"

ComputeHomography::ComputeHomography()
{

}

ComputeHomography::ComputeHomography(int nStreams, int mImagesInStream, int centerCamIndex, char **streamNames, char *clientName, char *dbName, char **camNames, int *camIds, char *outputLocation)
{
	_nStreams = nStreams;
	_mImagesPerStream = mImagesInStream;
	_centerCamIndex = centerCamIndex;
	GenerateOutputFileName(outputLocation, camNames);
	_metaData = PanoMetaData(nStreams, clientName, dbName, camNames, camIds);
	_homographies = vector<HomographyElement>(nStreams-1, HomographyElement());
	_allConverged = false;
	_result = PROCESS_NOT_STARTED;
	InitializeCalibrationData(nStreams, mImagesInStream, streamNames);	
}

void ComputeHomography::GenerateOutputFileName(char *outputLocation, char **camNames)
{
	string suffix = "";
	for(int i=0; i<_nStreams; i++)
		suffix += "_" + string(camNames[i]);

	_outputFile = string(outputLocation) + "PanoParameters" + suffix + ".xml"; 
}

void ComputeHomography::InitializeCalibrationData(int nStreams, int mImagesInStream, char **streamNames)
{
	for(int i=0; i<nStreams*mImagesInStream; i+=nStreams)
	{
		vector<string> streamnames;

		for(int j=0; j<3; j++)
			streamnames.push_back(string(streamNames[i+j]));
		
		_calibrationData.push_back(PanoCalibrationElement(streamnames, "", ""));

	}
}

ComputeHomography::~ComputeHomography()
{
	
}