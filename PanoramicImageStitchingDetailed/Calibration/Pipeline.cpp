#include "PanoCalibration.h"

void ComputeHomography::ComputeHomographies()
{
	vector<int> shuffledIndices = ShuffleIndices();
	
	for(int i=0; i<shuffledIndices.size() && !_allConverged; i++)
	{
		int index = shuffledIndices[i];
		
		vector<Mat> streams;
		if (ReadImages(_calibrationData[index], streams, _nStreams, true) != ElementStatus::LOADED_IMAGES)
		{
			BuildExceptionMessage(0, _calibrationData[index], HomographyStatus::CORRUPT_IMAGES);
			continue;
		}

		_allConverged = ComputeHomographyRow(_calibrationData[index], streams, _homographies);

		_imagesUsed.push_back(i);
		cout<<i<<"\t0\t"<<_homographies[0]._error<<endl;
		cout<<i<<"\t1\t"<<_homographies[1]._error<<endl<<endl;
	}
	//RecomputeHomography();
}

//void ComputeHomography::RecomputeHomography()
//{
//	Mat scaleMat = Mat::eye(3, 3, CV_64FC1);
//	newH
//}

void ComputeHomography::WriteParametersToFile()
{
	FileStorage fs(_outputFile, FileStorage::WRITE);
	WriteMetaData(fs);
	WriteImageNames(fs);
	WriteHomographies(fs);
	fs.release();
}

void ComputeHomography::ComputeCalibrationStatus()
{
	ComputeStatus();
	CreateExceptionMessage();
}

void ComputeHomography::CopyResults(double *stitchingParameters, char *exceptionMessage, int *exceptionMessageLength)
{
	//Copy stitching Parameters
	CopyHomographies(stitchingParameters);

	//Copy exception Message
	CopyExceptionMessage(exceptionMessage, exceptionMessageLength);
}