#include "opencv2\core\core.hpp"
#include "opencv2\nonfree\features2d.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\features2d\features2d.hpp"
#include "opencv2\calib3d\calib3d.hpp"
#include "PanoCommon.h"
#include <iostream>

using namespace PanoConfiguration;
using namespace cv;

typedef struct PanoElement PanoCalibrationElement;
typedef FileStatus HomographyStatus;

#pragma region HOMOGRAPHY STRUCT
struct HomographyElement
{
	vector<Point2f> _matchedRefKeypoints;
	vector<Point2f> _matchedSrcKeypoints;
	Mat _h;
	Mat _hPrev;
	double _error;
	bool _convergenceStarted;
	bool _converged;
	int _goodImagesCount;

	HomographyElement()
	{
		_h = Mat::eye(3, 3, CV_64FC1);
		_hPrev = Mat::eye(3, 3, CV_64FC1);
		_error = numeric_limits<double>::quiet_NaN();
		_convergenceStarted = false;
		_converged = false;
		_goodImagesCount =0;
	}
};
typedef struct HomographyElement HomographyElement;
#pragma endregion

#pragma region COMPUTE HOMOGRAPHY CLASS
class ComputeHomography
{
	private:

		#pragma region VARIABLES
		vector<PanoCalibrationElement> _calibrationData;
		
		int _nStreams;
		int _mImagesPerStream;
		int _centerCamIndex;
		
		vector<HomographyElement> _homographies;

		string _outputFile;
		PanoMetaData _metaData;
		vector<int> _imagesUsed;

		string _exceptionMessage;

		bool _allConverged;
		ProcessResult _result;
		#pragma endregion

		#pragma region METHODS

		#pragma region INITIALIZATION METHODS
		void GenerateOutputFileName(char *outputLocation, char **camNames);
		void InitializeCalibrationData(int nStreams, int mImagesInStream, char **streamNames);
		#pragma endregion

		#pragma region PIPELINE METHODS
		void ComputeHomographies();
		void WriteParametersToFile();
		void ComputeCalibrationStatus();
		void CopyResults(double *stitchingParameters, char *exceptionMessage, int *exceptionMessageLength);
		#pragma endregion

		#pragma region HOMOGRAPHY COMPUTATION METHODS
		vector<int> ShuffleIndices();
		bool ComputeHomographyRow(PanoCalibrationElement &pElement, vector<Mat> streams, vector<HomographyElement> &h);
		void IdentifyFrames(int j, vector<Mat> streams, Mat &refImage, Mat &srcImage);
		HomographyStatus CalculateHomography(Mat refImage, Mat srcImage, HomographyElement &hElement, bool isleft);
		void PreProcessImages(Mat refImage, Mat srcImage, bool isLeft, Mat &ppRefImage, Mat &ppSrcImage);
		HomographyStatus ExtractMatchingKeypoints(Mat refImage, Mat srcImage, vector<Point2f> &refMatchKeypoints, vector<Point2f> &srcMatchKeypoints);
		void RecomputeKeypoints(int originalWidth, vector<Point2f> &refMatchKeypoints, vector<Point2f> &srcMatchKeypoints, bool isLeft);
		void AppendMatchedPointsToGlobalList(vector<Point2f> refMatchKeypoints, vector<Point2f> srcMatchKeypoints, HomographyElement &hElement);
		HomographyStatus FindHomography(HomographyElement &hElement);
		HomographyStatus CheckForConvergence(HomographyElement &hElement);
		#pragma endregion
		
		#pragma region FILE OUTPUT METHODS
		void WriteMetaData(FileStorage fs);
		void WriteImageNames(FileStorage fs);
		void WriteHomographies(FileStorage fs);
		#pragma endregion

		#pragma region STATUS COMPUTATION METHODS 
		void ComputeStatus();
		void CreateExceptionMessage();
		#pragma endregion

		#pragma region RESULT COPY METHODS
		void CopyHomographies(double *stitchingParameters);
		void CopyExceptionMessage(char *exceptionMessage, int *exceptionMessageLength);
		#pragma endregion

		#pragma endregion

		int ComputeStitchingHomographies(double *stitchingParameters, char *exceptionMessage, int *exceptionMessageLength);

	public:
		#pragma region CONSTRUCTOR METHODS
		ComputeHomography();
		ComputeHomography(int nStreams, int mImagesInStream, int centerCamIndex, char **streamNames, char *clientName, char *dbName, char **camNames, int *camIds, char *outputLocation);
		~ComputeHomography();
		#pragma endregion
		
		int ComputeStitchingParameters(double *stitchingParameters, char *exceptionMessage, int *exceptionMessageLength);
};
#pragma endregion

#pragma region DLL METHODS
#ifdef _BUILD_DLL_
    #define IMPORT_EXPORT __declspec(dllexport) 
	#define NAME_CONV __cdecl	
#else
    #define IMPORT_EXPORT __declspec(dllimport)
	#define NAME_CONV 
#endif


extern "C"
{
	IMPORT_EXPORT int NAME_CONV ComputeStitchingParameters(	int nStreams, int nImagesInStream, int centerCamIndex, char **streamNames, double stitchingParameters[], 
															char *clientName, char *dbName, char **camNames, int *camIds, char *outputLocation, 
															char exceptionMessage[], int *exceptionMessageLength);
}
#pragma endregion