#include "PanoCalibration.h"

void ComputeHomography::WriteMetaData(FileStorage fs)
{
	fs<<"HEADER";
	fs<<"{";
			fs << "CREATED_BY" << COMPANY_NAME;
			fs << "CREATED_FOR" << _metaData._clientName;
			fs << "DATABASE_USED" << _metaData._dbName;
			fs << "NUM_IMAGE_STREAMS" << _nStreams;
			fs<< "CENTER_CAMERA_INDEX" << _centerCamIndex;
			fs << "STREAM_NAMES";
			fs<<"[";
					for (int i=1; i<=_nStreams; i++)
						fs << _metaData._camNames[i-1];
			fs <<"]";
			fs << "CAMERA_IDS";
			fs<<"[";
					for (int i=1; i<=_nStreams; i++)
						fs << _metaData._camIds[i-1];
			 fs<<"]";
	 fs<< "}";
}

void ComputeHomography::WriteImageNames(FileStorage fs)
{
	fs<<"IMAGES_USED";
	fs<<"[";
			for(int i=0; i<_imagesUsed.size(); i++)
				for(int j=0; j<_nStreams; j++)
					fs<<_calibrationData[_imagesUsed[i]].streamNames[j];
	fs<<"]";
}

void ComputeHomography::WriteHomographies(FileStorage fs)
{
	for(int i=0; i<_homographies.size(); i++)
	{
		string name = _metaData._camNames[i] + "_" + _metaData._camNames[i+1];
		fs<<name<<_homographies[i]._h;
	}
}