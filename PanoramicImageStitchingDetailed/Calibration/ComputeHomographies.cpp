#include "PanoCalibration.h"

static int corresNo = 0;

vector<int> ComputeHomography::ShuffleIndices()
{
	vector<int> indices;
	for(int i=0; i<_calibrationData.size(); i++)
		indices.push_back(i);
	
	random_shuffle(indices.begin(), indices.end());
	return indices;
}

bool ComputeHomography::ComputeHomographyRow(PanoCalibrationElement &element, vector<Mat> streams, vector<HomographyElement> &homographies)
{
	int homographyId = 0;
	bool allConverged = true;
	for(int streamIndex=0; streamIndex < _nStreams; streamIndex++)
	{			
		if(streamIndex==_centerCamIndex || homographies[homographyId]._converged) continue;

		Mat refImage, srcImage;
		IdentifyFrames(streamIndex, streams, refImage, srcImage);
		
		HomographyStatus status = CalculateHomography(refImage, srcImage, homographies[homographyId], (streamIndex < _centerCamIndex));
		BuildExceptionMessage(homographyId, element, status);

		allConverged = allConverged && homographies[homographyId]._converged;

		homographyId++;
	}
	return allConverged;
}

void ComputeHomography::IdentifyFrames(int streamIndex, vector<Mat> streams, Mat &refImage, Mat &srcImage)
{
	if (streamIndex < _centerCamIndex)
	{
		refImage = streams[streamIndex+1];
		srcImage = streams[streamIndex];
	}
	else
	{
		refImage = streams[streamIndex-1];
		srcImage = streams[streamIndex];
	}
}

HomographyStatus ComputeHomography::CalculateHomography(Mat refImage, Mat srcImage, HomographyElement &hElement, bool isLeft)
{		 
	Mat ppRefImage, ppSrcImage;
	PreProcessImages(refImage, srcImage, isLeft, ppRefImage, ppSrcImage);
	
	vector<Point2f> refMatchKeypoints, srcMatchKeypoints;
	if(ExtractMatchingKeypoints (ppRefImage, ppSrcImage, refMatchKeypoints, srcMatchKeypoints) != HomographyStatus::GOOD_MATCHES_FOUND)		
		return HomographyStatus::NO_GOOD_MATCH;

	//DisplayPlottedImage(ppRefImage, ppSrcImage, refMatchKeypoints, srcMatchKeypoints, "DownSampled");
	
	RecomputeKeypoints(refImage.cols, refMatchKeypoints, srcMatchKeypoints, isLeft);	
	
	//DisplayPlottedImage(refImage, srcImage, refMatchKeypoints, srcMatchKeypoints, "Original");
	//waitKey(0);

	AppendMatchedPointsToGlobalList(refMatchKeypoints, srcMatchKeypoints, hElement);

	if(FindHomography(hElement) != HomographyStatus::HOMOGRAPHY_COMPUTED) 
		return HomographyStatus::ERROR_COMPUTING_HOMOGRAPHY;

	if(CheckForConvergence(hElement) != HomographyStatus::CONVERGENCE_CHECKED)
		return HomographyStatus::CONVERGENCE_ERROR;

	return HomographyStatus::SUCCESS;
}

void ComputeHomography::PreProcessImages(Mat refImage, Mat srcImage, bool isLeft, Mat &ppRefImage, Mat &ppSrcImage)
{
	double nColsSliced = SEARCH_AREA_PERCENTAGE*refImage.cols;

	Rect leftEndRoi(0, 0, nColsSliced, refImage.rows);
	Rect rightEndRoi(refImage.cols - nColsSliced, 0, nColsSliced, refImage.rows);
	if(isLeft)
	{
		refImage(leftEndRoi).copyTo(ppRefImage);
		srcImage(rightEndRoi).copyTo(ppSrcImage);
	}
	else
	{
		refImage(rightEndRoi).copyTo(ppRefImage);
		srcImage(leftEndRoi).copyTo(ppSrcImage);
	}
	double scaleFactor = 1/DOWNLSAMPLE_FACTOR;
	resize(ppRefImage, ppRefImage, Size(), scaleFactor, scaleFactor);
	resize(ppSrcImage, ppSrcImage, Size(), scaleFactor, scaleFactor);
}


HomographyStatus ComputeHomography::ExtractMatchingKeypoints(Mat refImage, Mat srcImage, vector<Point2f> &refMatchKeypoints, vector<Point2f> &srcMatchKeypoints)
{
	vector<KeyPoint> keypointsRef, keypointsSrc;
	SiftFeatureDetector detector(5000);
	detector.detect(refImage, keypointsRef);
	detector.detect(srcImage, keypointsSrc);

	Mat descriptorsRef, descriptorsSrc;
	SiftDescriptorExtractor extractor;
    extractor.compute(refImage, keypointsRef, descriptorsRef);
    extractor.compute(srcImage, keypointsSrc, descriptorsSrc);
	
	FlannBasedMatcher matcher;
	vector<vector<DMatch>> matches;
	matcher.knnMatch( descriptorsRef, descriptorsSrc, matches, 4);

	vector<DMatch> goodMatches;
	for (int i = 0; i < matches.size(); ++i)
	{		
		if (matches[i][0].distance < KNN_MATCH_RATIO * matches[i][1].distance)
			goodMatches.push_back(matches[i][0]);
	}

	/*Mat corres = DrawFeatureMatches(refImage, srcImage, keypointsRef, keypointsSrc, goodMatches);
	string winName = "Matches";
	if(corresNo % 2  == 0)
		winName += "CenterLeft.jpg";
	else
		winName += " CenterRight.jpg";*/
	//namedWindow(winName)
	//imwrite(winName, corres);
	//if(corresNo == 1) waitKey(0);
	//else if(waitKey(30) >= 0) return HomographyStatus::NO_GOOD_MATCH;
	//corresNo++;
	//imwrite("E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\CALIBRATION\\Matches\\Corres " + to_string(corresNo++) + ".jpg", corres);

	if (goodMatches.size() < MIN_GOOD_FEATURES)
		return HomographyStatus::NO_GOOD_MATCH;

	for( int i = 0; i < goodMatches.size(); i++ )
	{
		refMatchKeypoints.push_back( keypointsRef[ goodMatches[i].queryIdx ].pt );
		srcMatchKeypoints.push_back( keypointsSrc[ goodMatches[i].trainIdx ].pt );
	}

	//if(corresNo % 2  != 0)
	//	_sleep(1000);
	
	//waitKey(0);

	return HomographyStatus::GOOD_MATCHES_FOUND;
}

void ComputeHomography::RecomputeKeypoints(int originalWidth, vector<Point2f> &refMatchKeypoints, vector<Point2f> &srcMatchKeypoints, bool isLeft)
{
	Mat scaleMat = Mat::eye(3, 3, CV_64FC1);
	scaleMat.at<double>(0,0) = DOWNLSAMPLE_FACTOR;
	scaleMat.at<double>(1,1) = DOWNLSAMPLE_FACTOR;
	perspectiveTransform(refMatchKeypoints, refMatchKeypoints, scaleMat);
	perspectiveTransform(srcMatchKeypoints, srcMatchKeypoints, scaleMat);

	double nColsToAdd = (1 - SEARCH_AREA_PERCENTAGE)*originalWidth;
	for(int i=0; i<refMatchKeypoints.size(); i++)
	{
		if(isLeft)
			srcMatchKeypoints[i].x += nColsToAdd;
		else
			refMatchKeypoints[i].x += nColsToAdd;
	}
}

void ComputeHomography::AppendMatchedPointsToGlobalList(vector<Point2f> refMatchKeypoints, vector<Point2f> srcMatchKeypoints, HomographyElement &hElement)
{
	hElement._matchedRefKeypoints.insert(hElement._matchedRefKeypoints.end(), refMatchKeypoints.begin(), refMatchKeypoints.end());
	hElement._matchedSrcKeypoints.insert(hElement._matchedSrcKeypoints.end(), srcMatchKeypoints.begin(), srcMatchKeypoints.end());
}

HomographyStatus ComputeHomography::FindHomography(HomographyElement &hElement)
{
	hElement._hPrev = hElement._h;

	hElement._h = findHomography(hElement._matchedSrcKeypoints, hElement._matchedRefKeypoints, CV_RANSAC, MAX_REPROJECTION_ERROR);

	//cout<<hElement._h<<endl;
	vector<Point2f> transformedPoints;
	perspectiveTransform(hElement._matchedSrcKeypoints, transformedPoints, hElement._h);

	int nErased = 0;
	for(int i=0; i<transformedPoints.size(); i++)
	{
		int newIndex = i - nErased;
		if(norm(transformedPoints[i]-hElement._matchedRefKeypoints[newIndex]) > MAX_REPROJECTION_ERROR)
		{
			hElement._matchedSrcKeypoints.erase(hElement._matchedSrcKeypoints.begin() + newIndex);
			hElement._matchedRefKeypoints.erase(hElement._matchedRefKeypoints.begin() + newIndex);
			nErased++;
		}
	}	

	if(hElement._matchedRefKeypoints.size() < MIN_RANSAC_INLIERS) 
		return HomographyStatus::ERROR_COMPUTING_HOMOGRAPHY; 

	hElement._h = findHomography(hElement._matchedSrcKeypoints, hElement._matchedRefKeypoints, CV_RANSAC, MAX_REPROJECTION_ERROR);
	//cout<<hElement._h<<endl;
	return HomographyStatus::HOMOGRAPHY_COMPUTED;
}

HomographyStatus ComputeHomography::CheckForConvergence(HomographyElement &hElement)
{	
	hElement._goodImagesCount++;

	if (hElement._convergenceStarted == false) 
	{
		hElement._convergenceStarted = true; 
		return HomographyStatus::CONVERGENCE_CHECKED;
	};
	
	hElement._error = norm(hElement._hPrev(Rect(0, 0, 2, 2)), hElement._h(Rect(0, 0, 2, 2)), NORM_L2);
	if(cvIsNaN(hElement._error)) 
		return HomographyStatus::CONVERGENCE_ERROR;

	hElement._converged = (hElement._error <= MAX_CONVERGANCE_ERROR && hElement._goodImagesCount > MIN_IMAGES_CONVERGANCE) ? true : false;
	return HomographyStatus::CONVERGENCE_CHECKED;
}