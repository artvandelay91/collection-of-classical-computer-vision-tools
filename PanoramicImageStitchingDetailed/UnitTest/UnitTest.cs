﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace UnitTest
{
    class UnitTest
    {
        static void Main(string[] args)
        {
            int nStreams = 3;
            int nImagesInStream = 568;
            //string inputPath = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\";
            //string outputPath = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\pano\\";
            //string inputPath = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\TEST2\\Input\\";
            //string outputPath = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\TEST2\\Output\\";
            string inputPath = "E:\\DATA\\Vision\\PanoProcessor\\ARAN\\set4\\";
            string outputPath = "E:\\DATA\\Vision\\PanoProcessor\\ARAN\\set4\\Output\\";
            bool createStreams = false;
            Test test = new Test (nStreams, nImagesInStream, inputPath, outputPath, createStreams);
            test.RunUnitTest();
        }
    }

    class Test
    {
        [DllImport("Roadware.Algorithm.PanoCalibration.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ComputeStitchingParameters(    int nStreams, int nImagesInStream, int centerCamIndex, string[] streamNames, double[] stitchingParameters, 
								                                string clientName, string dbName, string[] camNames, int[] camIds, string outputLocation, 
								                                byte[] exceptionMessage, ref int exceptionMessageLength );

        [DllImport("Roadware.Algorithm.PanoStitching.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int StitchImagesBatch(	int nStreams, int mImagesInStream, string[] streamNames, string[] camNames, int[] camIds, 
													string calibrationFile, string[] panoNames, byte[] exceptionMessage, ref int exceptionMessageLength	);

        [DllImport("VideoMockTest.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreatePanoramaTestSet( string videoPath, int nSplits, int overlapPercentage,
                                                        int rotation, int tx, int ty, int centerCamera, string savePath);


        private int _exceptionLength;
        private int _nStreams;
        private int _nImagesInStream;
        private string _inputPath;
        private string _outputPath;
        private static string _exceptionMessage;
        private List<string> _streamNames;
        private List<string> _panoNames;
        private double[] _homographies;


        public void RunUnitTest()
        {   
            string[] camNames = new string[] {"ROW1", "ROW2", "ROW3"};
            int[] camIds = new int[] {1, 2, 3};
            byte[] exceptionMessage = new byte[1000];
            int exceptionMessageLength = 0;
            bool aranCollection = false;
            if (aranCollection)
               GetAranStreamNames( ref _streamNames, ref _panoNames);
            _nImagesInStream = _panoNames.Count;
            int result = ComputeStitchingParameters(_nStreams, _nImagesInStream, 1, _streamNames.ToArray(), _homographies,
                                                                "CALTRANS", "CADOT", camNames, camIds, _outputPath,
                                                                exceptionMessage, ref exceptionMessageLength);
            
            string calibrationFile = _outputPath + "PanoParameters_ROW1_ROW2_ROW3.xml";
            //int result1 = StitchImagesBatch(_nStreams, _nImagesInStream, _streamNames.ToArray(), camNames, camIds, calibrationFile, _panoNames.ToArray(), exceptionMessage, ref exceptionMessageLength);

            string calibrationVideo = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\TEST2.mp4";
            string savePath = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\TEST2\\";
            //int result = CreatePanoramaTestSet(calibrationVideo, 3, 50, 10, 0, 0, 1, savePath);

            _exceptionMessage = System.Text.Encoding.UTF8.GetString(exceptionMessage, 0, exceptionMessageLength);
            System.Console.WriteLine(_exceptionMessage);
        }

        private void GetAranStreamNames(ref List<string> _streamNames, ref List<string> _panoNames)
        {
            _streamNames.Clear();
            _panoNames.Clear();

            string collectionPath = "E:\\DATA\\Vision\\PanoProcessor\\ARAN\\Collection1\\";
            string[] leftImages = Directory.GetFiles(collectionPath + "ForwardLeft");
            string[] centerImages = Directory.GetFiles(collectionPath + "ROW");
            string[] rightImages = Directory.GetFiles(collectionPath + "ForwardRight");
            int nImages = Math.Min(leftImages.Length, centerImages.Length);
            nImages = 10;//Math.Min(nImages, rightImages.Length);
            for (int i = 0; i < nImages; i++)
            {
                _streamNames.Add(leftImages[i]);
                _streamNames.Add(centerImages[i]);
                _streamNames.Add(rightImages[i]);
                _panoNames.Add(collectionPath + "Output\\Pano" + i.ToString() + ".jpg");
            }
        }


        public Test(int nStreams, int nImagesInStream, string inputPath, string outputPath, bool createStreams)
        {
            _exceptionLength = nStreams * nImagesInStream * 1000;
            _nStreams = nStreams;
            _nImagesInStream = nImagesInStream;
            _inputPath = inputPath;
            _outputPath = outputPath;
            _streamNames = new List<string>();
            _panoNames = new List<string>();
            _homographies = new double[(_nStreams - 1)*9];
            CreateStreams(createStreams);
        }

        private void CreateStreams(bool createStreams)
        {
            for (int i = 0; i <= _nImagesInStream-1; i++)
            {
                for (int j = 1; j <= _nStreams; j++)
                {
                    string ipName = _inputPath + j.ToString() + ".jpg";
                    string opPath = _inputPath + "Stream" + j.ToString(); 
                    string opName = opPath + "\\"+ i.ToString() + ".jpg";
                    if (!Directory.Exists(opPath))
                        Directory.CreateDirectory(opPath);
                    if (createStreams) File.Copy(ipName, opName, true);
                    _streamNames.Add(opName);

                    if (j > 1) continue;
                    if (!Directory.Exists(_outputPath))
                        Directory.CreateDirectory(_outputPath);
                    string panoName = _outputPath + i.ToString() + ".jpg";
                    _panoNames.Add(panoName);
                }
            }
        }

    }

}
