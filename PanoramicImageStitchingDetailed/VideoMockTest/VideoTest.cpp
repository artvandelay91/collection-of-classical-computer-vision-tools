#include "VideoTest.h"


int CreatePanoramaTestSet(char *videoPath, int nSplits, int overlapPercentage, int rotation, int tx, int ty, int centerCamera, char *savePath)
{
	Mat frame;

	//Open Video
	VideoCapture video;
	if(!OpenVideo(videoPath, &video))
		return -1;	

	//Compute where to split the frames
	video >> frame;
	//string imagePath = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\CALIBRATION\\Original\\";
	//frame = imread((imagePath+"0.jpg"), CV_LOAD_IMAGE_GRAYSCALE);
	vector<int> splitPoints = ComputeSplitPoints(frame, nSplits, overlapPercentage);

	//ComputeTransformation matrices
	vector<Mat> transformationMatrices = ComputeTransformationMatrices(	Mat(Size(splitPoints[1] - splitPoints[0], frame.rows), CV_8UC1, 0), 
																		nSplits, centerCamera, rotation, tx, ty);
	for(;;)
	{
	/*for(int i=0; i<14; i++)
    {*/		
		video >> frame;
		//frame = imread((imagePath + to_string(i)+ ".jpg"), CV_LOAD_IMAGE_GRAYSCALE);
		vector<Mat> splits = SplitIntoFrames(frame, nSplits, splitPoints, transformationMatrices);
		splits[centerCamera] = CropCenterFrame(splits[centerCamera], splits[0].size());
		Mat croppedFrame = frame;//CropWholeFrame(frame);		
		SaveFrames(splits, frame, croppedFrame, string(savePath));

#ifdef IMSHOW
		ShowFrames(frame, nSplits, splits);
		if(waitKey(30) >= 0) break;
#endif // IMSHOW
		
		videFrameNo++;
    }
 
    return 0;
}

void SaveFrames(vector<Mat> splits, Mat frame, Mat croppedFrame, string savePath)
{
	if(videFrameNo % saveFrequency != 0 ) return;
		
	cout<<"Saving frame "<< to_string(frameNo) << endl;
	string originalName = savePath + "Original\\" + to_string(frameNo) + ".jpg";
	imwrite(originalName, frame);

	string expectedName = savePath + "Expected\\" + to_string(frameNo) + ".jpg";
	imwrite(expectedName, croppedFrame);

	for(int i=0; i<splits.size(); i++)
	{
		string splitName = savePath + "Input\\Stream" + to_string(i+1) + "\\" + to_string(frameNo) + ".jpg";
		imwrite(splitName, splits[i]);
	}

	frameNo++;
}

void ShowFrames(Mat frame, int nSplits, vector<Mat> splits)
{
	for(int i=0; i<nSplits; i++)
	{
		resize(splits[i], splits[i], Size(), 0.5, 0.5);
		string winName = "Split " + to_string(i);
		imshow(winName, splits[i]);	
	}
	resize(frame, frame, Size(), 0.5, 0.5);
	imshow("frame", frame);	
}
vector<Mat> SplitIntoFrames(Mat frame, int nSplits, vector<int> splitPoints, vector<Mat>transformationMatrices)
{
	vector<Mat> splits;
	for(int i=0, j=0; i<nSplits && j<splitPoints.size(); i++, j+=2)
	{
		Mat split;
		frame(Range::all(), Range(splitPoints[j], splitPoints[j+1])).copyTo(split);		
		split = TransformFrame(split, transformationMatrices[i]);
		splits.push_back(split);
	}
	return splits;
}

Mat CropWholeFrame(Mat frame)
{
	frame = frame(Range(startHeight, endHeight), Range::all());
	return frame;
}

Mat CropCenterFrame(Mat frame, Size dstSize)
{
	Mat croppedFrame;
	int halfHeight = roundf(dstSize.height/2);
	int centerX = roundf(frame.cols/2);
	int centerY = roundf(frame.rows/2);
	startHeight = centerY - halfHeight;
	endHeight = centerY + halfHeight;
	frame(Range(startHeight, endHeight), Range::all()).copyTo(croppedFrame);
	resize(croppedFrame, croppedFrame, dstSize);
	return croppedFrame;
}


void PrintMats(vector<Mat> mats, int n)
{
	for(int i=0; i<n; i++)
		cout<<mats[i]<<endl<<endl;
}



bool OpenVideo(char *videoName, VideoCapture *video)
{
	*video = VideoCapture(videoName);
	if(!video->isOpened())
        return false;

	return true;
}



vector<int> ComputeSplitPoints(Mat frame, int nSplits, int overlapPercentage)
{
	int width = frame.cols;
	int splitWidth = floor((width * 100)/(100*nSplits - overlapPercentage*nSplits + overlapPercentage));
	vector<int> splitPoints;
	for(int i=0; i<nSplits; i++)
	{
		if(i == 0) { splitPoints.push_back(0); continue;}		
		int endSplit = splitPoints[splitPoints.size()-1] + splitWidth;
		int nextStart = floor(endSplit - (overlapPercentage * splitWidth / 100));
		splitPoints.push_back(endSplit);
		splitPoints.push_back(nextStart);
		if(i == nSplits-1) {splitPoints.push_back(width); break; }
	}

	return splitPoints;
}

#pragma region TRANSFORMATION COMPUTATION
vector<Mat> ComputeTransformationMatrices(Mat dummySplit, int nSplits, int centerCamera, int rotation, int tx, int ty)
{
	vector<Mat> transformationMats;
	for(int i=0; i<centerCamera; i++)
	{
		transformationMats.push_back(ComputeTransformationMatrix(dummySplit, rotation*(centerCamera - i), tx, ty, -1));
	}

	transformationMats.push_back(Mat::eye(3, 3, CV_32FC1));

	for(int i=nSplits-1; i>centerCamera; i--)
	{
		transformationMats.push_back(ComputeTransformationMatrix(dummySplit, rotation*(abs(i-centerCamera)), -tx, -ty, 1));
	}
	return transformationMats;
}

Mat ComputeTransformationMatrix(Mat dummySplit, int rotation, int xTranslation, int yTranslation, int side)
{
	return (ComputeTranslationMatrix(xTranslation, yTranslation) * ComputeRotationMatrix(dummySplit, rotation, side));
}

Mat ComputeTranslationMatrix(double tx, double ty)
{
	Mat translation = (Mat_<double>(3,3) <<	1, 0, tx, 
											0, 1, ty, 										
											0, 0, 1);
	return translation;
}

Mat ComputeRotationMatrix(Mat split, double rot, int side)
{
	rot = rot * PI / 180;
	
	vector<Point2f> src = GetSourcePoints(split);
	vector<Point2f> dst;

	if (side == -1)
		dst = GetTransformedPointsRight(split, rot);
	else
		dst = GetTransformedPointsLeft(split, rot);

	Mat H = findHomography(src, dst);
	
	return H;
}

vector<Point2f> GetSourcePoints(Mat split)
{
	vector<Point2f> src(4);
	src[0] = Point2f(0, 0);
	src[1] = Point2f(split.cols, 0);
	src[2] = Point2f(split.cols, split.rows);
	src[3] = Point2f(0, split.rows);
	return src;
}

vector<Point2f> GetTransformedPointsLeft(Mat split, double rot)
{
	vector<Point2f> dst(4);
	dst[0] =Point2f(0, 0);
	dst[1] = Point2f(ceilf(cos(rot)*split.cols), ceilf(sin(rot)*split.cols));
	dst[2] = Point2f(ceilf(cos(rot)*split.cols), ceilf(split.rows - sin(rot)*split.cols));
	dst[3] = Point2f(0, split.rows);
	return dst;
}

vector<Point2f> GetTransformedPointsRight(Mat split, double rot)
{
	vector<Point2f> dst(4);
	dst[0] = Point2f(split.cols - ceilf(cos(rot)*split.cols), ceilf(sin(rot)*split.cols));
	dst[1] = Point2f(split.cols, 0);
	dst[2] = Point2f(split.cols, split.rows);
	dst[3] = Point2f(split.cols - ceilf(cos(rot)*split.cols), ceilf(split.rows - sin(rot)*split.cols));
	return dst;
}
#pragma endregion

#pragma region TRANSFORMATION APPLICATION
Mat TransformFrame(Mat split, Mat H)
{
	warpPerspective(split, split, H, split.size());
	//imshow("image", split);
	//waitKey(0);

	/*vector<Point2f> src = GetSourcePoints(split);
	vector<Point2f> dst;
	perspectiveTransform(src, dst, H);

	int startRow = roundf(max(dst[0].y, dst[1].y));
	int endRow = roundf(min(dst[2].y, dst[3].y));
	int startCol  = roundf(max(dst[0].x, dst[3].x));
	int endCol = roundf(min(dst[1].x, dst[2].x));*/

	Mat croppedFrame = split;
	//split(Range(startRow, endRow) , Range(startCol, endCol)).copyTo(croppedFrame);
	return croppedFrame;
}

int roundf(float x)
{
   return x >= 0.0f ? floorf(x + 0.5f) : ceilf(x - 0.5f);
}
#pragma endregion