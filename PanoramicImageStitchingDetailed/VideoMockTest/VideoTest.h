#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\calib3d\calib3d.hpp"

#include <math.h>
#include <iostream>

using namespace cv;
using namespace std;


#pragma region DLL METHODS
#ifdef _BUILD_DLL_
    #define IMPORT_EXPORT __declspec(dllexport) 
	#define NAME_CONV __cdecl	
#else
    #define IMPORT_EXPORT __declspec(dllimport)
	#define NAME_CONV 
#endif

#define IMSHOW_

#define PI 3.14159265

static int startHeight = 0, endHeight = 0;
static int frameNo = 0;
static int videFrameNo = 0;
int saveFrequency = 10;

void PrintMats(vector<Mat> mats, int n);
bool OpenVideo(char *videoName, VideoCapture *video);
vector<int> ComputeSplitPoints(Mat frame, int nSplits, int overlapPercentage);
vector<Mat> ComputeTransformationMatrices(Mat dummySplit, int nSplits, int centerCamera, int rotation, int tx, int ty);
Mat ComputeTransformationMatrix(Mat dummySplit, int rotation, int xTranslation, int yTranslation, int side);
Mat ComputeRotationMatrix(Mat dummySplit, double rot, int side);
Mat ComputeTranslationMatrix(double tx, double ty);
vector<Point2f> GetTransformedPointsLeft(Mat split, double rot);
vector<Point2f> GetTransformedPointsRight(Mat split, double rot);
Mat TransformFrame(Mat split, Mat H);
vector<Point2f> GetSourcePoints(Mat split);
Mat ComputeRotationMatrixRight(Mat split, double rot);
int roundf(float x);
Mat CropCenterFrame(Mat frame, Size dstSize);
Mat CropWholeFrame(Mat frame);
vector<Mat> SplitIntoFrames(Mat frame, int nSplits, vector<int> splitPoints, vector<Mat>transformationMatrices);
void ShowFrames(Mat frame, int nSplits, vector<Mat> splits);
void SaveFrames(vector<Mat> splits, Mat frame, Mat croppedFrame, string savePath);

extern "C"
{
	IMPORT_EXPORT int NAME_CONV CreatePanoramaTestSet(char *videoPath, int nSplits, int overlapPercentage, int rotation, int tx, int ty, int centerCamera, char *savePath);
}
#pragma endregion