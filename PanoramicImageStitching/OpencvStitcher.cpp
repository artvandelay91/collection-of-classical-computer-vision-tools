#include < stdio.h >  
#include < opencv2\opencv.hpp >  
#include < opencv2\stitching\stitcher.hpp >

using namespace cv;  
using namespace std;


void main()  
{
 vector< Mat > vImg;
 Mat rImg;

 string path = "E:\\DATA\\Vision\\PanoProcessor\\ARAN\\Set1\\";

 vImg.push_back( imread(path + "1.jpg") );
 vImg.push_back( imread(path + "2.jpg") );
 vImg.push_back( imread(path + "3.jpg") );

 Stitcher stitcher = Stitcher::createDefault();

 unsigned long AAtime=0, BBtime=0; //check processing time
 AAtime = getTickCount(); //check processing time

 Stitcher::Status status = stitcher.stitch(vImg, rImg);

 BBtime = getTickCount(); //check processing time 
 printf("%.2lf sec \n",  (BBtime - AAtime)/getTickFrequency() ); //check processing time

 if (Stitcher::OK == status) 
  imwrite(path + "panorama.jpg", rImg);
  else
  printf("Stitching fail.");

 //waitKey(0);

}  
/////