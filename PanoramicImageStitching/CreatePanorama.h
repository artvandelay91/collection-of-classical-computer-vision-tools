#include "opencv2\opencv.hpp"
#include "opencv2\nonfree\features2d.hpp"
#include "Config.h"

using namespace cv;
using namespace std;

void CreatePanorama(string leftName, string rightName, string panoName);

void PreprocessImages(Mat &leftImage, Mat &rightImage);
void ExtractMatchingKeypoints(Mat leftImage, Mat rightImage, vector<Point2f> &leftImagePoints, vector<Point2f> &rightImagePoints);
void RecomputeCorrespondences(Size originalSize, vector<Point2f> &leftImagePoints, vector<Point2f> &rightImagePoints);
void ComputeHomography(Mat leftImage, Mat rightImage, Mat &H);

void ComputeProjectedCorners(Size singleSize, Mat H, vector<Point2f> &projectedCorners);
void CreateCanvasForPanorama(Size singleSize, int imageType, vector<Point2f> projectedCorners, Point2f &leftStartPoint, Mat &canvas);
void OverlayImageOnCanvas(Mat leftImage, Mat rightImage, Point2f leftTopCorner, Mat H, Mat &panorama);
void CropPanorama(vector<Point2f> projectedCorners, Point2f lefImageTopCorner, Size singleSize, Mat &panorama);
void CreatePanorama(Mat leftImage, Mat rightImage, Mat H, Mat &panorama);

void SaveImage(string imageName, Mat image);

void  ShowImage(string winName, Mat image, int waitkey)
{
	namedWindow(winName, CV_WINDOW_KEEPRATIO);
	imshow(winName, image);
}