#include "opencv2\opencv.hpp"
#include "opencv2\nonfree\features2d.hpp"
#include "Config.h"

using namespace cv;
using namespace std;

//Relative path where the images are stored
const string ImagePath = "..\\set1\\";
const string PanoPath = "..\\results\\";

//OPTIMIZATION PARAMETER
//Percentage of the images that are of interest (TARGETED SEARCH)
//If the images overlap by only by X percentage, there is is no point in searching the entire image
//Since the right image is projected onto the left image. Right X% of the left image and left X% of the right image alone are enough
//If this is not know before hand, should be set to 1 (100%).
const double SEARCH_AREA_PERCENTAGE = 0.6;

//OPTIMIZATION PARAMETER
//Downsampling the images improves the speed of stitching. 
//The final size of the images will be 1/DOWNSAMPLE_FACTOR on width and height.
const double DOWNSAMPLE_FACTOR = 4;

//DETECTION PARAMETER
//Set the number of features to be detected by the SIFT detector. (OpenCV Parameter)
//More the value, more the time taken
const int MAX_SIFT_FEATURES = 1000;

//DETECTION PARAMETER
//Set the number of nearest neighbors to be compared against for the KNN search in the FLANN matcher (OpenCV Parameter)
const int NUM_KNN_NEIGHBORS = 4;

//DETECTION PARAMETER
//The ratio of distances between the first and second best match in feature matching.
//Lower values retain the best matches
//0.8 is the value D.Lowe suggets in his paper
const double KNN_MATCH_RATIO = 0.8;

#pragma region METHODS
void PanoramaProcess(string leftName, string rightName, string panoName);

void PreprocessImages(Mat &leftImage, Mat &rightImage);
void ExtractMatchingKeypoints(Mat leftImage, Mat rightImage, vector<Point2f> &leftImagePoints, vector<Point2f> &rightImagePoints);
void RecomputeCorrespondences(Size originalSize, vector<Point2f> &leftImagePoints, vector<Point2f> &rightImagePoints);
void ComputeHomography(Mat leftImage, Mat rightImage, Mat &H);

void ComputeProjectedCorners(Size singleSize, Mat H, vector<Point2f> &projectedCorners);
void CreateCanvasForPanorama(Size singleSize, int imageType, vector<Point2f> projectedCorners, Point2f &leftStartPoint, Mat &canvas);
void OverlayImageOnCanvas(Mat leftImage, Mat rightImage, Point2f leftTopCorner, Mat H, Mat &panorama);
void CropPanorama(vector<Point2f> projectedCorners, Point2f lefImageTopCorner, Size singleSize, Mat &panorama);
void CreatePanorama(Mat leftImage, Mat rightImage, Mat H, Mat &panorama);

void SaveImage(string imageName, Mat image);
#pragma endregion

#pragma region HOMOGRAPHY
void PreprocessImages(Mat &leftImage, Mat &rightImage)
{	
	//Convert the images to greyscale
	cvtColor(leftImage, leftImage, CV_BGR2GRAY);
	cvtColor(rightImage, rightImage, CV_BGR2GRAY);

	//Slice only the Regions of Interest
	double nColsSliced = SEARCH_AREA_PERCENTAGE*leftImage.cols;
	Rect leftEndRoi(0, 0, nColsSliced, leftImage.rows);
	Rect rightEndRoi(leftImage.cols - nColsSliced, 0, nColsSliced, leftImage.rows);
	leftImage = leftImage(rightEndRoi);
	rightImage = rightImage(leftEndRoi);

	//Downsample the Regions of Interest
	double scaleFactor = 1/DOWNSAMPLE_FACTOR;
	resize(leftImage, leftImage, Size(), scaleFactor, scaleFactor);
	resize(rightImage, rightImage, Size(), scaleFactor, scaleFactor);	
}

void ExtractMatchingKeypoints(Mat leftImage, Mat rightImage, vector<Point2f> &leftImagePoints, vector<Point2f> &rightImagePoints)
{
	//Extract SIFT KeyPoints
	vector<KeyPoint> keypointsLeft, keypointsRight;
	SiftFeatureDetector detector(MAX_SIFT_FEATURES);
	detector.detect(leftImage, keypointsLeft);
	detector.detect(rightImage, keypointsRight);

	//Extract the SIFT features the keypoints
	Mat descriptorsRef, descriptorsSrc;
	SiftDescriptorExtractor extractor;
    extractor.compute(leftImage, keypointsLeft, descriptorsRef);
    extractor.compute(rightImage, keypointsRight, descriptorsSrc);
	
	//Match the features with a Flann KNN matcher
	FlannBasedMatcher matcher;
	vector<vector<DMatch>> matches;
	matcher.knnMatch( descriptorsRef, descriptorsSrc, matches, NUM_KNN_NEIGHBORS);

	//Compute the good matches determined by the ratio of distances between best and secon best matches
	vector<DMatch> goodMatches;
	for (int i = 0; i < matches.size(); ++i)
	{		
		if (matches[i][0].distance < KNN_MATCH_RATIO * matches[i][1].distance)
			goodMatches.push_back(matches[i][0]);
	}

	//Retain only the good matches
	for( int i = 0; i < goodMatches.size(); i++ )
	{
		leftImagePoints.push_back( keypointsLeft[ goodMatches[i].queryIdx ].pt );
		rightImagePoints.push_back( keypointsRight[ goodMatches[i].trainIdx ].pt );
	}
}

void RecomputeCorrespondences(Size originalSize, vector<Point2f> &leftImagePoints, vector<Point2f> &rightImagePoints)
{
	//Rescale the points to original size from the preprossed size
	Mat scaleMat = Mat::eye(3, 3, CV_64FC1);
	scaleMat.at<double>(0,0) = DOWNSAMPLE_FACTOR;
	scaleMat.at<double>(1,1) = DOWNSAMPLE_FACTOR;
	perspectiveTransform(leftImagePoints, leftImagePoints, scaleMat);
	perspectiveTransform(rightImagePoints, rightImagePoints, scaleMat);

	//Add the width of the left image that was cropped out in the preprocess stage
	double nColsToAdd = (1 - SEARCH_AREA_PERCENTAGE)*originalSize.width;
	for(int i=0; i<leftImagePoints.size(); i++)
			leftImagePoints[i].x += nColsToAdd;
}

void ComputeHomography(Mat leftImage, Mat rightImage, Mat &H)
{	
	//Step 0 - Preprocess the images
	Size originalSize = leftImage.size();
	PreprocessImages(leftImage, rightImage);

	//Step 1 - Compute the matching keypoints in both images
	vector<KeyPoint> keypointsLeft, keypointsRight;
	vector<Point2f> leftImagePoints, rightImagePoints;
	ExtractMatchingKeypoints(leftImage, rightImage, leftImagePoints, rightImagePoints);

	//Step 2 - Recompute the location of keypoints in the original image
	RecomputeCorrespondences(originalSize, leftImagePoints, rightImagePoints);

	//Step 3 - Compute homography using RANSAC
	H = findHomography(rightImagePoints, leftImagePoints, CV_RANSAC, 1.0);
}
#pragma endregion

#pragma region STITCH IMAGES
//Computes the corners of the right image when projected onto left image
void ComputeProjectedCorners(Size singleSize, Mat H, vector<Point2f> &projectedCorners)
{
	vector<Point2f> src(4);
	src[0] = Point2f(0, 0);
	src[1] = Point2f(singleSize.width, 0);
	src[2] = Point2f(singleSize.width, singleSize.height);
	src[3] = Point2f(0, singleSize.height);	
	perspectiveTransform(src, projectedCorners, H);
}

//Computes the location of the top left corner of the left image in the final panorama
//Creates the canvas for the creating the panorama
void CreateCanvasForPanorama(Size singleSize, int imageType, vector<Point2f> projectedCorners, Point2f &leftStartPoint, Mat &canvas)
{
	//Canvas for the creating the panorama
	double maxX = 0.0, minY = DBL_MAX;
	for(int i=0; i<projectedCorners.size(); i++)
	{
		if(projectedCorners[i].x > maxX) maxX = projectedCorners[i].x;
		if(projectedCorners[i].y < minY) minY = projectedCorners[i].y;
	}
	minY = (minY > 0) ? 0 : minY; 
	maxX = (maxX < singleSize.width) ? singleSize.width : maxX; 

	double canvasHeight = singleSize.height - minY;
	double canvasWidth = maxX;

	canvas = Mat(canvasHeight, canvasWidth, imageType, 0);
	
	//Top left corner of the left image 
	leftStartPoint = Point2f(0, (minY < 0) ? -minY : 0);
}

//Produces panorama
void OverlayImageOnCanvas(Mat leftImage, Mat rightImage, Point2f leftTopCorner, Mat H, Mat &panorama)
{
	//Overlay the right image on the canvas
	warpPerspective(rightImage, panorama, H, panorama.size());

	//Overlay the left image on the canvas
	leftImage.copyTo(panorama(Rect(leftTopCorner.x, leftTopCorner.y, leftImage.cols, leftImage.rows)));

	//ShowImage("Panorama", panorama, 1);
	//waitKey(0);
}

//Crop out the empty regions and retain only the proper image
void CropPanorama(vector<Point2f> projectedCorners, Point2f lefImageTopCorner, Size singleSize, Mat &panorama)
{
	double topLeftCornerX = 0;
	double topLEftCornerY = lefImageTopCorner.y;

	double nCols = min(projectedCorners[1].x, projectedCorners[2].x);
	double nRows = min(projectedCorners[2].y, projectedCorners[3].y);
	nRows = min((double)singleSize.height, nRows) - topLEftCornerY;
	panorama = panorama(Rect(topLeftCornerX, topLEftCornerY, nCols, nRows));

	//ShowImage("Panorama", panorama, 1);
	//waitKey(0);
}

void CreatePanorama(Mat leftImage, Mat rightImage, Mat H, Mat &panorama)
{
	//Step 1 - Compute the corners of the right image when projected onto left image
	vector<Point2f> projectedCorners(4);
	ComputeProjectedCorners(leftImage.size(), H, projectedCorners);

	//Step 2 - a. Compute the location of the top left corner of the left image
	//		   b. Create the canvas for the creating the panorama
	Point2f lefImageTopCorner;
	CreateCanvasForPanorama(leftImage.size(), leftImage.type(), projectedCorners, lefImageTopCorner, panorama);
	
	//Step 3 - Let and right images on the canvas
	OverlayImageOnCanvas(leftImage, rightImage, lefImageTopCorner, H, panorama);

	//Step 4 - Crop out the empty regions and retain only the proper image
	CropPanorama(projectedCorners, lefImageTopCorner, leftImage.size(), panorama);
}

// Saves the created Panorama
void SaveImage(string imageName, Mat image)
{
	imwrite(imageName, image);
}
#pragma endregion

void PanoramaProcess(string leftName, string rightName, string panoName)
{
	//Load the images
	Mat leftImage = imread(leftName);
	Mat rightImage = imread(rightName);

	//Compute projective homography between the views
	Mat H;
	ComputeHomography(leftImage, rightImage, H);

	//Create panorama with the computed Homography
	Mat panorama;
	CreatePanorama(leftImage, rightImage, H, panorama);
	
	//Save the created panorama to disk
	SaveImage(panoName, panorama);
}

int main()
{
	string leftName = ImagePath + "img1.jpg"; 
	string rightName = ImagePath + "img2.jpg";
	
	string panoName = PanoPath + "panorama.jpg";

	PanoramaProcess(leftName, rightName, panoName);

	return 0;
}