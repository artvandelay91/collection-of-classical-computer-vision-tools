#include "opencv2\opencv.hpp"
#include <opencv2/nonfree/features2d.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include "opencv2\nonfree\nonfree.hpp"
using namespace std;
using namespace cv;

const int nStreams = 3;
static int nImages = 0;
const int minDistFactor = 3;
const int minGoodFeatures = 20;
const double convergenceError = 5E-3; 
const int centerCamIndex = 1;

struct Homography
{
	unsigned char refId;
	unsigned char srcId;
	vector<Point2f> matchedRefKeypoints;
	vector<Point2f> matchedSrcKeypoints;
	Mat H;
	double error;
	bool convergenceStarted;
	bool converged;

	Homography()
	{
		refId = 0;
		srcId = 0;
		H = Mat::eye(3, 3, CV_64FC1);
		error = DBL_MAX;
		convergenceStarted = false;
		converged = false;
	}
};
typedef struct Homography Homography;


void StitchImagesBatch(string inputLocation, string outputLocation);

void GetImageNames(string inputLocation, string opLocation, vector<string> &ipImages, vector<string> &opImages);
void CalculateHomographies(vector<string> ipImages);
void CreatePanorama(vector<string> ipImages, vector<string> opImages);

void ReadImages(vector<string> ipImages, int i, vector<Mat> &streams, bool isGrey);	
void IdentifyFrameIds(int j, int hId, Homography &H);
void CalculateHomography(vector<Mat> streams, Homography &h);
void WriteHomographyToFile(vector<Homography> h);

void IdentifyFrames(vector<Mat> streams, Mat &refImage, Mat &srcImage, Homography h);
bool ExtractMatchingKeypoints(Mat refImage, Mat srcImage, Homography &h);
void FindHomography(Homography &h);
void CheckForConvergence(Mat hCurrent, Homography &h);

void ReadHomographyFromFile(vector<Mat> &H);
void StitchImages(vector<string> ipImages, vector<Mat> H);


void IdentifyFrameIdsForStitching(int j, vector<Mat> streams, Mat pano, Mat &refImage, Mat &srcImage);
void StitchConsecutiveImages(Mat refImage, Mat srcImage, Mat h, bool isLeft, Mat &pano);

void ComputeSourceFrameCorners(Mat srcImage, Mat h, vector<Point2f> &dst);
void RecomputeHomography(Mat refImage, Mat srcImage, vector<Point2f> dst, bool isLeft, Mat &h);
void CreateCanvasForResult(vector<Point2f> dst, Mat refImage, Mat srcImage, bool isLeft, Mat &result);
void OverlayImagesOnCanvas(Mat srcImage, Mat refImage, Mat h, vector<Point2f> dst, bool isLeft, Mat &result);

int main()
{
	
	string inputLocation = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\CALIBRATION\\Input\\";
	string outputLocation = "E:\\DATA\\Vision\\PanoProcessor\\VideoTest\\CALIBRATION\\Output\\";

	StitchImagesBatch(inputLocation, outputLocation);

	return 0;
}

void StitchImagesBatch(string inputLocation, string outputLocation)
{
	vector<string> ipImages, opImages; 	
	GetImageNames(inputLocation, outputLocation, ipImages, opImages);
	
	//CalculateHomographies(ipImages);

	CreatePanorama(ipImages, opImages);
}

void GetImageNames(string inputLocation, string opLocation, vector<string> &ipImages, vector<string> &opImages)
{
	ifstream infile(inputLocation + "fileNames.txt");
	string line;
	getline(infile, line);
	getline(infile, line);	
	for (int i=0; i<3; i++)
		getline(infile, line);
	while(getline(infile, line) && line != "")
		ipImages.push_back(line);
	nImages = ipImages.size()/3;
	for(int i=0; i<nImages; i++)
		opImages.push_back(opLocation+"Pano"+to_string(i)+".jpg");
	
}

#pragma region COMPUTE HOMOGRAPHY
void CalculateHomographies(vector<string> ipImages)
{	
	vector<Homography> H(nStreams-1, Homography());
	for(int i=0; i<nImages; i++)
	{
		vector<Mat> streams;
		ReadImages(ipImages, i, streams, true);		
		
		int hId = 0;
		for(int j=0; j<nStreams; j++)
		{			
			if(j==centerCamIndex) continue;

			IdentifyFrameIds(j, hId, H[hId]);

			CalculateHomography(streams, H[hId]);

			cout<<i<<'\t'<<hId<<'\t'<<H[hId].error<<endl;
			
			hId++;	
		}
		cout<<endl;
	}
	WriteHomographyToFile(H);
}

void ReadImages(vector<string> ipImages, int n, vector<Mat> &streams, bool isGrey)
{
	for(int i =0; i<nStreams; i++)
	{
		string imName =  ipImages[n+(i*nImages)];
		unsigned int loadOption = (isGrey) ? CV_LOAD_IMAGE_GRAYSCALE : CV_LOAD_IMAGE_COLOR; 
		Mat image = imread(imName, loadOption);
		streams.push_back(image);
	}
}

void IdentifyFrameIds(int j, int hId, Homography &H)
{
	if(!(H.refId == 0 && H.srcId == 0)) return;

	if (j < centerCamIndex)
	{
		H.refId = j+1;
		H.srcId = j;
	}
	else
	{
		H.refId = j-1;
		H.srcId = j;
	}
}

void CalculateHomography(vector<Mat> streams, Homography &h)
{
	Mat refImage, srcImage;
	IdentifyFrames(streams, refImage, srcImage, h);

	if(!ExtractMatchingKeypoints (refImage, srcImage, h))
		return;
	
	Mat hCurrent = h.H;
	FindHomography(h);
	
	CheckForConvergence(hCurrent, h);
}

void IdentifyFrames(vector<Mat> streams, Mat &refImage, Mat &srcImage, Homography h)
{
	refImage = streams[h.refId];
	srcImage = streams[h.srcId];
}

bool ExtractMatchingKeypoints(Mat refImage, Mat srcImage, Homography &h)
{
	vector<KeyPoint> keypointsRef, keypointsSrc;
	SiftFeatureDetector detector(400);
	detector.detect(refImage, keypointsRef);
	detector.detect(srcImage, keypointsSrc);

	Mat descriptorsRef, descriptorsSrc;
	SiftDescriptorExtractor extractor;
    extractor.compute(refImage, keypointsRef, descriptorsRef);
    extractor.compute(srcImage, keypointsSrc, descriptorsSrc);

	vector<DMatch> goodMatches;
	FlannBasedMatcher matcher;
	vector<DMatch> matches(min(descriptorsRef.total(), descriptorsSrc.total()), DMatch());
	matcher.match( descriptorsRef, descriptorsSrc, matches);	
	double max_dist = 0; double min_dist = 100;
	for( int i = 0; i < descriptorsRef.rows; i++ )
	{ 
		double dist = matches[i].distance;
		if( dist < min_dist ) min_dist = dist;
		if( dist > max_dist ) max_dist = dist;
	}
	for( int i = 0; i < descriptorsRef.rows; i++ )
	{ 
		if( matches[i].distance <= max(minDistFactor*min_dist, 0.02) )
			goodMatches.push_back(matches[i]);
	}

	if (goodMatches.size() < minGoodFeatures) return false;

	for( int i = 0; i < goodMatches.size(); i++ )
	{
		h.matchedRefKeypoints.push_back( keypointsRef[ goodMatches[i].queryIdx ].pt );
		h.matchedSrcKeypoints.push_back( keypointsSrc[ goodMatches[i].trainIdx ].pt );
	}
	return true;
}

void FindHomography(Homography &h)
{
	h.H = findHomography(h.matchedSrcKeypoints, h.matchedRefKeypoints, CV_RANSAC, 1.0);
	cout << h.H <<endl;

	vector<Point2f> transformedPoints;
	perspectiveTransform(h.matchedSrcKeypoints, transformedPoints, h.H);
	vector<double> errors;
	for(int i=0; i<transformedPoints.size(); i++)
		errors.push_back(norm(transformedPoints[i]-h.matchedRefKeypoints[i]));

	int nErased = 0;
	for(int i=0; i<transformedPoints.size(); i++)
	{
		if(norm(transformedPoints[i]-h.matchedRefKeypoints[i - nErased]) > 0.1)
		{
			h.matchedSrcKeypoints.erase(h.matchedSrcKeypoints.begin() + (i - nErased));
			h.matchedRefKeypoints.erase(h.matchedRefKeypoints.begin() + (i - nErased));
			nErased++;
		}
	}

	h.H = findHomography(h.matchedSrcKeypoints, h.matchedRefKeypoints, CV_RANSAC, 1.0);
	cout << h.H <<endl;
}

void CheckForConvergence(Mat hCurrent, Homography &h)
{	
	if (h.convergenceStarted == false) {h.convergenceStarted = true; return;};
	
	h.error = norm(hCurrent(Rect(0, 0, 3, 2)), h.H(Rect(0, 0, 3, 2)), NORM_L2);	
	h.converged = (h.error <= convergenceError) ? true : false;
}

void WriteHomographyToFile(vector<Homography> h)
{
	FileStorage fs("homography.xml", FileStorage::WRITE);
	fs<<"H1"<<h[0].H;
	fs<<"H2"<<h[1].H;
	fs.release();
}
#pragma endregion

void CreatePanorama(vector<string> ipImages, vector<string> opImages)
{
	vector<Mat> H;
	ReadHomographyFromFile(H);

	StitchImages(ipImages, H);
}

void StitchImages(vector<string> ipImages, vector<Mat> H)
{
	for(int i=0; i<nImages; i++)
	{
		vector<Mat> streams;
		ReadImages(ipImages, i, streams, false);

		int hId = 0;
		Mat pano;
		for(int j=0; j<nStreams; j++)
		{
			if(j == centerCamIndex) continue;
			
			Mat refImage, srcImage;
			IdentifyFrameIdsForStitching(j, streams, pano, refImage, srcImage);

			StitchConsecutiveImages(refImage, srcImage, H[hId], (j<centerCamIndex), pano);
			hId++;
		}
	}
}

void ReadHomographyFromFile(vector<Mat> &H)
{
	FileStorage fs;
	fs.open("homography.xml", FileStorage::READ);
	Mat h1, h2;
	fs["H1"] >> h1; 
	H.push_back(h1);
	fs["H2"] >> h2; 
	H.push_back(h2);
	fs.release();
}

void IdentifyFrameIdsForStitching(int j, vector<Mat> streams, Mat pano, Mat &refImage, Mat &srcImage)
{
	if(j < centerCamIndex)
	{
		refImage = streams[j+1];
		if (pano.rows == 0)
			srcImage = streams[j];
		else
			srcImage = pano;
	}
	else
	{
		srcImage = streams[j];
		if(pano.rows==0)
			refImage = streams[j-1];
		else
			refImage = pano;
	}
}

void StitchConsecutiveImages(Mat refImage, Mat srcImage, Mat h, bool isLeft, Mat &result)
{	
	
	vector<Point2f> dst(4);
	ComputeSourceFrameCorners(srcImage, h, dst);

	RecomputeHomography(refImage, srcImage, dst, isLeft, h);

	CreateCanvasForResult(dst, refImage, srcImage, isLeft, result);

	OverlayImagesOnCanvas(srcImage, refImage, h, dst, isLeft, result);
	
	namedWindow("Result", CV_WINDOW_KEEPRATIO);
	imshow( "Result", result );
	waitKey(0);
}

void ComputeSourceFrameCorners(Mat srcImage, Mat h, vector<Point2f> &dst)
{
	vector<Point2f> src(4);
	src[0] = Point2f(0, 0);
	src[1] = Point2f(srcImage.cols, 0);
	src[2] = Point2f(srcImage.cols, srcImage.rows);
	src[3] = Point2f(0, srcImage.rows);
	
	perspectiveTransform(src, dst, h);
	
	for(int i=0; i<dst.size(); i++)
	{
		dst[i].x = floorf(dst[i].x);
		dst[i].y = floorf(dst[i].y);
	}
}

void RecomputeHomography(Mat refImage, Mat srcImage, vector<Point2f> dst, bool isLeft, Mat &h)
{
	
	Mat newH = Mat::eye(3, 3, h.type());
	if (isLeft)
		newH.at<double>(0, 2) = -dst[0].x;
	else
		newH.at<double>(0, 2) = refImage.cols - srcImage.cols;

	h = newH * h;
}

void CreateCanvasForResult(vector<Point2f> dst, Mat refImage, Mat srcImage, bool isLeft, Mat &result)
{
	double overlap = (isLeft) ? srcImage.cols - (-dst[0].x) : srcImage.cols - dst[0].x; //for right shouldn't be srcImage.cols
	double canvasWidth = refImage. cols + srcImage.cols - overlap;
	result = Mat(refImage.rows, canvasWidth, refImage.type(), 0);
}

void OverlayImagesOnCanvas(Mat srcImage, Mat refImage, Mat h, vector<Point2f> dst, bool isLeft, Mat &result)
{
	warpPerspective(srcImage,result, h, result.size());
	if(isLeft)
		refImage.copyTo(result(Rect(-dst[0].x, 0, refImage.cols, refImage.rows)));
	else
		refImage.copyTo(result(Rect(0, 0, refImage.cols, refImage.rows)));
}