#include "ImageQCAlgorithm.h"


ImageQCAlgorithm::ImageQCAlgorithm(){}

ImageQCAlgorithm::ImageQCAlgorithm(char *imagePath, char *extn)
{
	imageName = string(imageName);
	extension = string(extn);
}

ImageQCAlgorithm::~ImageQCAlgorithm(){}

int ImageQCAlgorithm::ReadImage()
{
	if (extension==".tiff" || extension == ".tif")
		return ReadTiffImage();
	else if (extension == ".jpeg" || extension == ".jpg" || extension == ".png")
		return ReadJpegImage();
	else
		return UNRECOGNIZED_FILE_FORMAT;
}

int ImageQCAlgorithm::ReadTiffImage()
{
	//TIFF *tif = TIFFOpen(imageName.c_str(), "r");	

	//if (tif) 
	//{
	//	//get number of rows and cols
	//	int nRows, nCols;
	//	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &nRows);
	//	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &nCols);

	//	//Read the layer data scanline by scanline
	//	uint8 *layerData = (uint8*) _TIFFmalloc(nRows*nCols * sizeof (uint8));		
	//	uint8 *rowData = (uint8*) _TIFFmalloc(nCols *sizeof(uint8));
	//	for (int row = 0; row < nRows; row++)
	//	{
	//		TIFFReadScanline(tif, rowData, row);
	//		memcpy(layerData + row*nCols, rowData, nCols*sizeof(uint8));
	//	}
	//		
	//	//create a mat with the layer data
	//	frame = Mat(nRows, nCols, CV_8UC1, layerData);		
	//		
	//	//Free the memory and close	
	//	_TIFFfree(layerData);
	//	_TIFFfree(rowData);
	//	TIFFClose(tif);	
	//}
	//else
	//	return CORRUPT_IMAGE;
	return S_OK;
}

int ImageQCAlgorithm::ReadJpegImage()
{
	frame = imread(imageName, CV_LOAD_IMAGE_GRAYSCALE);
	if (!frame.data)
		return CORRUPT_IMAGE;
	return S_OK;
}

int ImageQCAlgorithm::CalculateImageBrightness()
{
	Scalar meanIntensity = mean(frame);
	imageBrightness = meanIntensity[0];
	if (imageBrightness <=0 || cvIsNaN(imageBrightness))
		return CORRUPT_IMAGE;
	return S_OK;
}

int ImageQCAlgorithm::GetImageBrightness(double *imageIntensity)
{
	int result = CalculateImageBrightness();
	*imageIntensity = imageBrightness;
	return result;
}

int ComputeImageBrightness(char* imagePath, char *extension, double *imageIntensity)
{
	ImageQCAlgorithm qc(imagePath, extension);
	return qc.GetImageBrightness(imageIntensity);
}

void ResizeImage(char** imagePath, char **extn, int n)
{
	for(int i=0; i<n; i++)
	{
		std::cout<<i<<std::endl;
		string imName = string(imagePath[i]) + string(extn[i]);
		Mat image = imread(imName, CV_LOAD_IMAGE_GRAYSCALE);
		resize(image, image, Size(200, 200));
		string saveName = string(imagePath[i]) + ".jpg";
		imwrite(saveName, image);
	}
}