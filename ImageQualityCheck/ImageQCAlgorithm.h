#include <opencv/cv.h>
#include <opencv/highgui.h>
//#include "tiffio.h"
#include <iostream>
#include <string>

using namespace cv;

#define DLLEXPORT __declspec( dllexport )
#define NAME_CONV __cdecl

class ImageQCAlgorithm
{
	private:
		string imageName, extension;
		Mat frame;
		double imageBrightness;

		int ReadImage();
		int ReadTiffImage();
		int ReadJpegImage();
		int CalculateImageBrightness();

		enum ERROR_CODES
		{
			S_OK = 0,
			UNRECOGNIZED_FILE_FORMAT = 1,
			CORRUPT_IMAGE = 2
		};

	public:
		ImageQCAlgorithm(char* imagePath, char *extension);
		ImageQCAlgorithm();
		~ImageQCAlgorithm();
		int GetImageBrightness(double *imageIntensity);
};

extern "C"
{
	//DLLEXPORT int NAME_CONV ComputeImageBrightness(char* imagePath, char *extension, double *intensity);
	DLLEXPORT void NAME_CONV ResizeImage(char** imagePath, char **extn, int n);
}
